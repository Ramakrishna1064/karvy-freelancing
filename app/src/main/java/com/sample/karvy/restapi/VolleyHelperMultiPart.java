package com.sample.karvy.restapi;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sample.karvy.R;
import com.sample.karvy.common.ApplicationBase;
import com.sample.karvy.common.UserNotification;
import com.sample.karvy.utils.Constants;
import com.sample.karvy.utils.Utilities;

import org.json.JSONObject;

import java.io.File;
import java.util.List;
import java.util.Map;


/**
 * Created by krishna on 11/25/2016.
 */
public class VolleyHelperMultiPart {

    static ProgressDialog progressDialog = null;

    /**
     * @param context
     * @param serverUrl
     * @param params
     * @param fileMap
     * @param reqType
     */
    public static void postMultipartMultipleFiles(final Context context, final String serverUrl,
                                                  final Map<String, String> params,
                                                  final Map<String, String> authenticationHeaders,
                                                  final Map<String, List<File>> fileMap, final String reqType) {
        System.out.println("Headers are---->" + params);
        System.out.println("fileMap are---->" + fileMap);
        try {

            if (NetWorkConnection.isConnected()) {

                progressDialog = Utilities.showProgressDialog(context);
                progressDialog.show();

                final RequestQueue queue = Volley.newRequestQueue(context.getApplicationContext());
                //final String url = ServerUrls.BASE_URL + serverUrl;
                final MultipartAPI multipartAPI = new MultipartAPI(context, serverUrl, params, authenticationHeaders, fileMap,
                        volleyError -> {
                            System.out.println("Error response is--->" + volleyError.getMessage());
                            if (progressDialog != null) {
                                progressDialog.dismiss();
                                progressDialog = null;
                            }
                            if (volleyError != null) {

                                if (VolleyErrorHelper.getMessage(volleyError) != null) {

                                    NetworkResponse response = volleyError.networkResponse;
                                    if (response.statusCode == 401) {
                                        Utilities.showSessionExpiredDialog(context);
                                    } else {
                                        String errorMessage = VolleyErrorHelper.getMessage(volleyError);
                                        UserNotification.notify(reqType, errorMessage);
                                    }
                                } else {
                                    Utilities.showAlertMessage(Constants.VOLLEY_ERROR_SERVER, context);
                                }
                            } else {
                                Utilities.showAlertMessage(Constants.VOLLEY_ERROR_SERVER, context);
                            }
                        }, response -> {
                    System.out.println("Success response is--->" + response);
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                    try {
                        JSONObject responseObj = new JSONObject(response);
                        if (responseObj.has("errors")) {
                            // System.out.println("Expire your token");
                        } else {
                            UserNotification.notify(reqType, response);
                            UserNotification.notifyFragment(reqType, response);
                        }
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });

                int socketTimeout = 450000;
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                multipartAPI.setRetryPolicy(policy);
                queue.add(multipartAPI);

            } else {
                //Show the Network Error Dialog
                Utilities.showAlertMessage(ApplicationBase.getContext().
                        getResources().getString(R.string.network_connection_error), context);
            }

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
