package com.sample.karvy.restapi;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sample.karvy.R;
import com.sample.karvy.common.ApplicationBase;
import com.sample.karvy.common.UserNotification;
import com.sample.karvy.common.UserPreferences;
import com.sample.karvy.utils.Constants;
import com.sample.karvy.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.channels.AcceptPendingException;
import java.util.HashMap;
import java.util.Map;


public class VolleyHelperPost {

    private ProgressDialog progressDialog;

    /**
     * callVolleyHelperPost
     *
     * @param requestUrl
     * @param json
     * @param reqType
     * @param context
     * @param
     */
    public void callVolleyHelperPost(final String requestUrl, final String json,
                                     final String reqType,
                                     final Context context) {
        try {
            if (NetWorkConnection.isConnected()) {
                progressDialog = Utilities.showProgressDialog(context);
                progressDialog.show();
                // Get a RequestQueue
                RequestQueue queue = Volley.newRequestQueue(context.getApplicationContext());
                StringRequest request = new StringRequest(Request.Method.POST, requestUrl, response -> {
                    System.out.println("Final one response:--->" + response);
                    try {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                            progressDialog = null;
                        }
                        JSONObject responseObj = new JSONObject(response);
                        if (responseObj.has("errors")) {
                        } else {
                            UserNotification.notify(reqType, response);
                            UserNotification.notifyFragment(reqType, response);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, volleyError -> {
                    System.out.println("Failure response:--->" + volleyError);
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                    if (volleyError != null) {
                        if (VolleyErrorHelper.getMessage(volleyError) != null) {
                            NetworkResponse response = volleyError.networkResponse;
                            if (response.statusCode == 401) {
                                Utilities.showSessionExpiredDialog(context);
                            } else {
                                String errorMessage = VolleyErrorHelper.getMessage(volleyError);
                                UserNotification.notify(reqType, errorMessage);
                            }
                        } else {
                            Utilities.showAlertMessage(Constants.VOLLEY_ERROR_SERVER, context);
                        }
                    } else {
                        Utilities.showAlertMessage(Constants.VOLLEY_ERROR_SERVER, context);
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        try {
                            Map<String, String> hashMap = new HashMap<>();
                            String ACCESS_TOKEN=UserPreferences.getPreferenceString(Constants.ATHERIZATION_TOKEN, null);
                            hashMap.put("Authorization", "Bearer " +ACCESS_TOKEN );
                            return hashMap;
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    public byte[] getBody() {
                        try {
                            if (json != null) {
                                return json.getBytes(Constants.CHARSET_UTF_8);
                            }
                        } catch (Throwable t) {
                            t.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    public String getBodyContentType() {
                        return Constants.CONTENT_TYPE;
                    }
                };

                request.setRetryPolicy(new DefaultRetryPolicy(120000,
                        0, 1.5f));
                request.setShouldCache(false);
                queue.add(request);

            } else {
                //Show the Network Error Dialog
                Utilities.showAlertMessage(ApplicationBase.getContext().getResources()
                        .getString(R.string.network_connection_error), context);
            }

        } catch (Throwable t) {
            t.printStackTrace();
            return;
        }
    }
}
