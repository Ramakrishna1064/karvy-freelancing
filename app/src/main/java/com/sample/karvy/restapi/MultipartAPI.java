package com.sample.karvy.restapi;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.sample.karvy.common.UserPreferences;
import com.sample.karvy.utils.Constants;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by krishna on 11/24/2016.
 */

public class MultipartAPI extends Request<String> {

    private MultipartEntityBuilder mBuilder;
    private final Response.Listener<String> mListener;
    protected Map<String, String> msecureHeaders;
    private Map<String, String> mParams;
    private Map<String, List<File>> mFileParams;
    private String mBodyContentType;
    private Context context;

    public MultipartAPI(Context context, String url, final Map<String, String> params, final Map<String, String> secureHeaders,
                        final Map<String, List<File>> fileParams,
                        Response.ErrorListener errorListener, Response.Listener<String> listener) {

        super(Method.POST, url, errorListener);
        mListener = listener;
        mParams = params;
        this.context=context;
        msecureHeaders = secureHeaders;
        mFileParams = fileParams;
        mBuilder = MultipartEntityBuilder.create();
        buildMultipartEntity();
    }


    /**
     * AuthFailureError
     *
     * @return
     */
    @Override
    public Map<String, String> getHeaders() {
        try {
            Map<String, String> hashMap = new HashMap<>();
            String ACCESS_TOKEN=UserPreferences.getPreferenceString(Constants.ATHERIZATION_TOKEN, null);
            hashMap.put("Authorization", "Bearer " +ACCESS_TOKEN );
            return hashMap;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    /**
     * @return
     */
    @Override
    public String getBodyContentType() {
        return mBodyContentType;
    }

    @Override
    public byte[] getBody() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            HttpEntity entity = mBuilder.build();
            mBodyContentType = entity.getContentType().getValue();
            entity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream bos, building the multipart request.");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        String parsed;
        try {
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            parsed = new String(response.data);
        }
        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }

    /**
     *
     */
    private void buildMultipartEntity() {

        if (mParams != null) {
            for (Map.Entry<String, String> entry : mParams.entrySet()) {
                mBuilder.addTextBody(entry.getKey(), entry.getValue());
            }
        }

        if (mFileParams != null) {
            for (Map.Entry<String, List<File>> entry : mFileParams.entrySet()) {
                List<File> listFiles = entry.getValue();
                for (File file : listFiles) {
                        mBuilder.addBinaryBody(entry.getKey(), file, ContentType.create("image/jpg"),
                                file.getName());
                }
            }
        }
        mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
    }
}
