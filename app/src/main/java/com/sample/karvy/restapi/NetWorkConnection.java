package com.sample.karvy.restapi;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.sample.karvy.utils.Constants;

public class NetWorkConnection {

    private static Context _Context;

    public static void init(Context context) {
        _Context = context;
    }

    /**
     *notifyConnectivityStatus
     */
    public static void notifyConnectivityStatus() {
        String status = getConnectivityStatus();
        //UserNotification.notify(Constants.NETWORK_STATUS, status);
    }

    /**
     * @return
     */
    public static boolean isConnected() {
        boolean isConnected = false;

        try {
            ConnectivityManager cm = (ConnectivityManager) _Context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            isConnected = activeNetwork != null
                    && activeNetwork.isConnected();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return isConnected;
    }

    /**
     * @return
     */
    public static String getConnectivityStatus()

    {
        ConnectivityManager cm = (ConnectivityManager) _Context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork)

        {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return Constants.NETWORK_TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return Constants.NETWORK_TYPE_MOBILE;

        }
        return Constants.NETWORK_TYPE_NOT_CONNECTED;
    }

    /**
     * @return
     */
    public static String getConnectivityStatusString()
    {
        String conn = com.sample.karvy.restapi.NetWorkConnection.getConnectivityStatus();
        String status = null;
        if (conn == Constants.NETWORK_TYPE_WIFI) {
            status = Constants.NETWORK_STRING_WIFI;
        } else if (conn == Constants.NETWORK_TYPE_MOBILE) {
            status =Constants.NETWORK_STRING_MOBILE;
        } else if (conn == Constants.NETWORK_TYPE_NOT_CONNECTED) {
            status =Constants.NETWORK_STRING_NOT_CONNECTED;
        }
        return status;
    }
}
