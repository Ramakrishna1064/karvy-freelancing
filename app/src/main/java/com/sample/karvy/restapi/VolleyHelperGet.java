package com.sample.karvy.restapi;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sample.karvy.R;
import com.sample.karvy.common.UserNotification;
import com.sample.karvy.common.UserPreferences;
import com.sample.karvy.utils.AppController;
import com.sample.karvy.utils.Constants;
import com.sample.karvy.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class VolleyHelperGet {

    static ProgressDialog progressDialog = null;

    /**
     * @param requestUrl
     * @param iheaders
     * @param reqType
     * @param context
     */
    public  void callVolleyHelperGet(final String requestUrl, final Map<String, String> iheaders,
                                     final String reqType, final Context context) {
        try {

            //System.out.println("url is====>" + requestUrl);
            if (com.sample.karvy.restapi.NetWorkConnection.isConnected()) {

                if (progressDialog == null) {
                    progressDialog = Utilities.showProgressDialog(context);
                    progressDialog.show();
                }
                // Get a RequestQueue
                RequestQueue queue = Volley.newRequestQueue(context.getApplicationContext());
                // String url = ServerUrls.BASE_URL + requestUrl;
                //Log.i("URL:", "URL:--->" + requestUrl);
                StringRequest request = new StringRequest(Request.Method.GET, requestUrl, response -> {

                   // System.out.println("Success response:--->" + response);
                    try {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                            progressDialog = null;
                        }
                        JSONObject responseObj = new JSONObject(response);

                        if (responseObj.has("errors")) {
                            try {

                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        } else {
                            UserNotification.notify(reqType, response);
                            UserNotification.notifyFragment(reqType, response);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, volleyError -> {
                   // System.out.println("Errror response:--->" + volleyError.getMessage());
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                    NetworkResponse response = volleyError.networkResponse;
                    if(response == null)
                        Utilities.showAlertMessage(AppController.getContext().getResources()
                                .getString(R.string.network_connection_error), context);
                    else
                    if (response.statusCode == 401) {
                        Utilities.showSessionExpiredDialog(context);
                    } else {
                        Utilities.showAlertMessage(Constants.VOLLEY_ERROR_SERVER, context);
                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        try{
                            Map<String, String> hashMap = new HashMap<>();
                            hashMap.put("Authorization", "Bearer " + UserPreferences.getPreferenceString(Constants.ATHERIZATION_TOKEN,null));
                            return hashMap;
                        }catch (Throwable throwable){
                            throwable.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    public String getBodyContentType() {
                        return Constants.CONTENT_TYPE;
                    }
                };
                request.setRetryPolicy(new DefaultRetryPolicy(120000, 0, 1.5f));
                request.setShouldCache(false);
                queue.add(request);

            } else {
                //Show the Network Error Dialog
                Utilities.showAlertMessage(AppController.getContext().
                        getResources().getString(R.string.network_connection_error), context);
            }

        } catch (Throwable t) {
            t.printStackTrace();
            return;
        }
    }
}
