package com.sample.karvy.restapi;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


public class VolleyInit {

    private static com.sample.karvy.restapi.VolleyInit _Instance;
    private static Context _Context;
    private RequestQueue _RequestQueue;

    private VolleyInit(Context context) {
        _Context = context;
        _RequestQueue = getRequestQueue();
    }

    public static synchronized com.sample.karvy.restapi.VolleyInit getInstance() {
        if (_Instance == null) {
          //  _Instance = new VolleyInit(AppController.getContext());
        }
        return _Instance;
    }

    public RequestQueue getRequestQueue() {
        if (_RequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            _RequestQueue = Volley.newRequestQueue(_Context.getApplicationContext());
        }
        return _RequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}
