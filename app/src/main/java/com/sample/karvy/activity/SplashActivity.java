package com.sample.karvy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.sample.karvy.R;
import com.sample.karvy.common.ActivityBase;
import com.sample.karvy.common.UserPreferences;
import com.sample.karvy.utils.Constants;

public class SplashActivity extends ActivityBase {

    private int userid = 0;

    /**
     * onCreate
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setupLayoutBase(
                    R.layout.activity_splash);
            hideActionBar();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (UserPreferences.getPreferenceString(Constants.ATHERIZATION_TOKEN, null) != null) {
                        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        //Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }, 2000);

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}