package com.sample.karvy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.sample.karvy.R;
import com.sample.karvy.common.ActivityBase;
import com.sample.karvy.common.UserPreferences;
import com.sample.karvy.restapi.VolleyHelperLoginPost;
import com.sample.karvy.utils.AsteriskPasswordTransformation;
import com.sample.karvy.utils.Constants;
import com.sample.karvy.utils.ServerUrls;
import com.sample.karvy.utils.Utilities;

import org.json.JSONObject;

public class LoginActivity extends ActivityBase implements View.OnClickListener, TextWatcher {

    private TextInputLayout userNameInput, passwordInput;
    private EditText userName, password;
    private TextView version;

    /**
     * onCreate
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupLayoutBase(R.layout.activity_login_layout);
        hideActionBar();
        initializingView();
    }

    /**
     * initializingView
     */
    private void initializingView() {
        try {
            userNameInput = findViewById(R.id.user_name_input);
            userName = findViewById(R.id.user_name);
            userName.addTextChangedListener(this);
            passwordInput = findViewById(R.id.password_input);
            password = findViewById(R.id.password);
            password.addTextChangedListener(this);

            version = findViewById(R.id.version);
            version.setText("Version "+Utilities.getVersionName(this));

            findViewById(R.id.login).setOnClickListener(this);
            findViewById(R.id.password_show).setOnClickListener(this);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * onClick
     *
     * @param v
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.login:
                validateInputFields();
                break;

            case R.id.password_show:
                if (((CheckBox) v).isChecked()) {
                    password.setTransformationMethod(null);
                } else {
                    password.setTransformationMethod(new AsteriskPasswordTransformation());
                }
                password.setSelection(password.getText().length());
                break;
        }
    }


    /**
     * validateInputFields
     */
    private void validateInputFields() {

        try {
            boolean isValidate = true;
            if (userName.getText().toString().equalsIgnoreCase("")) {
                new Utilities().setErrorToTextInputLayout(userNameInput, getString(R.string.mobilenumber_10digits));
                isValidate = false;
            }
            if (password.getText().toString().equalsIgnoreCase("")) {
                new Utilities().setErrorToTextInputLayout(passwordInput, getString(R.string.password_required));
                isValidate = false;
            }
            if (userName.getText().toString().trim().length() < 10
                    || userName.getText().toString().trim().length() > 10) {
                new Utilities().setErrorToTextInputLayout(userNameInput, getString(R.string.mobilenumber_10digits));
                isValidate = false;
            }

            if (isValidate) {
                submitDetailsToServer();
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * submitDetailsToServer
     */
    private void submitDetailsToServer() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Mobile", userName.getText().toString().trim());
            jsonObject.put("UserEnccryptedPassword", password.getText().toString().trim());
            new VolleyHelperLoginPost().callVolleyHelperPost(ServerUrls.LOGIN_TOKEN, jsonObject.toString(),
                    Constants.NOTIFICATION_LOGIN_REQUEST, this);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * validateLoginResponse
     *
     * @param response
     */
    private void validateLoginResponse(String response) {
        try {
            if (response != null) {
                System.out.println("Final response is--->" + response);
                JSONObject jsonObject = new JSONObject(response);
                boolean isStatus = jsonObject.getBoolean("Status");
                if (isStatus) {
                    UserPreferences.setPreferenceInt(Constants.USER_ID,
                            jsonObject.getJSONObject("Result").getInt("UsrId"));
                    UserPreferences.setPreferenceString(Constants.ATHERIZATION_TOKEN,
                            jsonObject.getJSONObject("Result").getString("Token"));
                    Intent intent = new Intent(this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Utilities.showAlertMessage(jsonObject.getString("Messege"), this);
                }
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * onReceiveNotification
     *
     * @param notificationType
     * @param notificationMessage
     */
    @Override
    public void onReceiveNotification(String notificationType, String notificationMessage) {
        super.onReceiveNotification(notificationType, notificationMessage);

        switch (notificationType) {
            case Constants.NOTIFICATION_LOGIN_REQUEST:
                validateLoginResponse(notificationMessage);
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (!(s.toString().equals(""))) {
            //set the Error as null values when user typing:-
            Utilities.setEmptyError(userName, s, userNameInput);
            Utilities.setEmptyError(password, s, passwordInput);
        }
    }
}
