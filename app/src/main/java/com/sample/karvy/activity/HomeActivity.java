package com.sample.karvy.activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;
import com.sample.karvy.R;
import com.sample.karvy.adapters.HomeListAdapter;
import com.sample.karvy.common.ActivityBase;
import com.sample.karvy.common.UserPreferences;
import com.sample.karvy.restApiModel.SubmittedRecordsList;
import com.sample.karvy.restApiModel.SubmittedRecordsResult;
import com.sample.karvy.restapi.VolleyHelperPost;
import com.sample.karvy.roomDb.db.PDQuaQuaDBClient;
import com.sample.karvy.roomDb.entities.UserEntity;
import com.sample.karvy.utils.Constants;
import com.sample.karvy.utils.Conversions;
import com.sample.karvy.utils.ServerUrls;
import com.sample.karvy.utils.Utilities;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HomeActivity extends ActivityBase implements View.OnClickListener {

    private FloatingActionButton fabButton;
    private RecyclerView homeRecyclerView;
    private TextView textCartItemCount;
    private List<SubmittedRecordsList> submittedRecordsListList = new ArrayList<>();
    private HomeListAdapter homeListAdapter;
    private TextView currentDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupLayoutBase(R.layout.activity_home);
        setActionBarTitle("Karvy", false, false);
        initializingView();
        // new GetAsyncTask().execute();
    }

    /**
     *
     */
    private void initializingView() {
        try {
            //Call Submitted records List
            currentDate = findViewById(R.id.current_date);
            currentDate.setText("Current Date: " + SimpleDateFormat.getDateInstance().format(new Date()));
            fabButton = findViewById(R.id.fab_icon);
            fabButton.setOnClickListener(this);
            homeRecyclerView = findViewById(R.id.home_recyclerview);
            homeRecyclerView.setHasFixedSize(true);
            homeRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            homeListAdapter = new HomeListAdapter(this, submittedRecordsListList);
            homeRecyclerView.setAdapter(homeListAdapter);
            JSONObject jsonObject = new JSONObject();
            new VolleyHelperPost().callVolleyHelperPost(ServerUrls.FORM_SUBMIT_RECORDS_LIST_URL, jsonObject.toString(),
                    Constants.NOTIFICATION_SUBMITTED_RECORDS_LIST_REQUEST, this);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_icon:
                navigateToCreateFormActivity();
                break;
        }
    }

    /**
     * onReceiveNotification
     *
     * @param notificationType
     * @param notificationMessage
     */
    @Override
    public void onReceiveNotification(String notificationType, String notificationMessage) {
        super.onReceiveNotification(notificationType, notificationMessage);

        switch (notificationType) {
            case Constants.NOTIFICATION_SUBMITTED_RECORDS_LIST_REQUEST:
                System.out.println("Notification response is--->"+notificationMessage);
                validateSubmittedRecordsResponse(notificationMessage);
                break;
        }
    }

    private void navigateToCreateFormActivity() {
        Intent intent = new Intent(this, FormActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_cart);
        final MenuItem menuItem_logout = menu.findItem(R.id.action_logout);
        View actionView = menuItem.getActionView();
        View actionView_logout = menuItem_logout.getActionView();
        textCartItemCount = actionView.findViewById(R.id.cart_badge);
        new GetAsyncTask().execute();
        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });
        actionView_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem_logout);
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart:
                Intent intent = new Intent(this, OfflineRecordsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                Utilities.callLoginActivity(this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // new GetAsyncTask().execute();
    }

    /**
     *
     */
    private class GetAsyncTask extends AsyncTask<Void, Void, List<UserEntity>> {

        @Override
        protected List<UserEntity> doInBackground(Void... voids) {
            List<UserEntity> list = PDQuaQuaDBClient.getInstance(HomeActivity.this)
                    .getAppDatabase()
                    .userDao()
                    .getAllRecords();
            return list;
        }

        @Override
        protected void onPostExecute(List<UserEntity> userEntities) {
            super.onPostExecute(userEntities);
            //Toast.makeText(HomeActivity.this, "Offline data count is" + userEntities.size(), Toast.LENGTH_SHORT).show();
            if (userEntities != null && userEntities.size() > 0)
                setupBadge(userEntities.size());
            //menuItem.setIcon(buildCounterDrawable(userEntities.size(), R.drawable.cloud_upload));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void setupBadge(int mCartItemCount) {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 10000)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    /**
     * validateSubmittedRecordsResponse
     *
     * @param response
     */
    private void validateSubmittedRecordsResponse(String response) {
        if (response != null) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                boolean isStatus = jsonObject.getBoolean("Status");
                if (isStatus) {
                    SubmittedRecordsResult submittedRecordsResult = (SubmittedRecordsResult)
                            Conversions.JsonToObject(response, SubmittedRecordsResult.class);
                    submittedRecordsListList.addAll(submittedRecordsResult.getResult());
                    homeListAdapter.notifyDataSetChanged();
                } else {
                    Utilities.showAlertMessage(jsonObject.getString("Messege"), this);
                }
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }
}