package com.sample.karvy.activity;

import androidx.arch.core.internal.FastSafeIterableMap;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sample.karvy.R;
import com.sample.karvy.adapters.OfflineListAdapter;
import com.sample.karvy.common.ActivityBase;
import com.sample.karvy.model.FormActivityDetailsModel;
import com.sample.karvy.restapi.NetWorkConnection;
import com.sample.karvy.restapi.VolleyHelperMultiPart;
import com.sample.karvy.roomDb.db.PDQuaQuaDBClient;
import com.sample.karvy.roomDb.entities.UserEntity;
import com.sample.karvy.utils.Constants;
import com.sample.karvy.utils.ServerUrls;
import com.sample.karvy.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OfflineRecordsActivity extends ActivityBase implements View.OnClickListener {
    private RecyclerView offlineRecyclerView;
    private Button syncButton;
    private List<FormActivityDetailsModel> userOfflineData;
    private OfflineListAdapter adapter;
    private TextView noRecords;

    private int intialPosition = 0;
    private int totalPositions = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupLayoutBase(R.layout.activity_offline_records);
        setActionBarTitle(getResources().getString(R.string.sync_records), true, false);
        intialiseViews();
    }

    /**
     *
     */
    private void intialiseViews() {
        try {
            syncButton = findViewById(R.id.sync_details);
            syncButton.setOnClickListener(this);

            noRecords = findViewById(R.id.no_records);

            offlineRecyclerView = findViewById(R.id.offline_recyclerview);
            offlineRecyclerView.setHasFixedSize(true);
            offlineRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            new GetAsyncTask().execute();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sync_details:
                totalPositions = userOfflineData.size();
                // deleteRecordsFromRoomDatabase(intialPosition);
                if (NetWorkConnection.isConnected())
                    syncDetailsToServer();
                else
                    Utilities.showAlertMessage("Your still offline please enable internet connectivity",
                            this);
                break;
        }
    }

    /**
     * deleteRecordsFromRoomDatabase
     */
    private void deleteRecordsFromRoomDatabase(int position) {
        if (position < totalPositions) {
            new DeleteRecordsFromDb(position).execute();
        }
    }

    /**
     *
     */
    private class GetAsyncTask extends AsyncTask<Void, Void, List<UserEntity>> {

        @Override
        protected List<UserEntity> doInBackground(Void... voids) {
            List<UserEntity> list = PDQuaQuaDBClient.getInstance(OfflineRecordsActivity.this)
                    .getAppDatabase()
                    .userDao()
                    .getAllRecords();
            return list;
        }

        @Override
        protected void onPostExecute(List<UserEntity> userEntities) {
            super.onPostExecute(userEntities);
            System.out.println("--->" + userEntities.size());
            userOfflineData = new ArrayList<>();

            if (userEntities.size() == 0) {
                noRecords.setVisibility(View.VISIBLE);
                offlineRecyclerView.setVisibility(View.GONE);
                syncButton.setEnabled(false);
                syncButton.setClickable(false);
                return;
            }
            Type type = new TypeToken<FormActivityDetailsModel>() {
            }.getType();


            if (userEntities != null && userEntities.size() > 0) {
                for (int i = 0; i < userEntities.size(); i++) {
                    FormActivityDetailsModel model = new Gson().fromJson(userEntities.get(i).getUserData().toString(), type);
                    model.setUserId(userEntities.get(i).getId());
                    userOfflineData.add(model);
                }
            }
            adapter = new OfflineListAdapter(OfflineRecordsActivity.this, userOfflineData);
            offlineRecyclerView.setAdapter(adapter);
        }
    }

    private class DeleteRecordsFromDb extends AsyncTask<Void, Void, Void> {
        int position;

        public DeleteRecordsFromDb(int position) {
            this.position = position;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            if (new File(userOfflineData.get(position).getNew_meter_image()).exists()) {
                if (new File(userOfflineData.get(position).getNew_meter_image()).delete()) {
                }
            }
            if (new File(userOfflineData.get(position).getLatest_bill_image()).exists()) {
                if (new File(userOfflineData.get(position).getLatest_bill_image()).delete()) {
                }
            }

            PDQuaQuaDBClient.getInstance(OfflineRecordsActivity.this)
                    .getAppDatabase()
                    .userDao()
                    .deleteById(userOfflineData.get(position).getUserId());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //userOfflineData.remove(intialPosition);
            //adapter.notifyItemRemoved(intialPosition);

            intialPosition = intialPosition + 1;
            if (intialPosition < totalPositions)
                syncDetailsToServer();
            else {
                Toast.makeText(OfflineRecordsActivity.this, "All records are synced successfully", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(OfflineRecordsActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }
    }

    /**
     *
     */
    private void syncDetailsToServer() {
        try {
            //Set Headers
            Map<String, String> headers = new HashMap<String, String>();
            headers.put("NewMeter_Number", userOfflineData.get(intialPosition).getNewMettorNo());
            headers.put("ConsumerNumber", userOfflineData.get(intialPosition).getkNo());
            headers.put("OldMeter_Number", userOfflineData.get(intialPosition).getOldMeterSerilalNo());
            headers.put("OldMeter_Reading", userOfflineData.get(intialPosition).getOldMeterReading());
            headers.put("BoxSeal1", userOfflineData.get(intialPosition).getBoxSealNo1());
            headers.put("BoxSeal2", userOfflineData.get(intialPosition).getBoxSealNo2());
            headers.put("Address", userOfflineData.get(intialPosition).getCustomerAddress());
            headers.put("MobileNumber", userOfflineData.get(intialPosition).getMobileNumber());
            headers.put("Latitude", "" + userOfflineData.get(intialPosition).getLatitude());
            headers.put("Longitude", "" + userOfflineData.get(intialPosition).getLongitude());
            headers.put("InstalledDate", userOfflineData.get(intialPosition).getInstallationDate());

            HashMap<String, List<File>> fileMap = new HashMap();
            //Set Images
            List<File> NewMeterImagesFileList = new ArrayList<>();
            NewMeterImagesFileList.add(new File(userOfflineData.get(intialPosition).getNew_meter_image()));
            fileMap.put("newMeterImage", NewMeterImagesFileList);

            List<File> BillImagesFileList = new ArrayList<>();
            BillImagesFileList.add(new File(userOfflineData.get(intialPosition).getLatest_bill_image()));
            fileMap.put("latest_Ele_BillImage", BillImagesFileList);

            new VolleyHelperMultiPart().postMultipartMultipleFiles(this, ServerUrls.FORM_SUBMIT_URL, headers,
                    null,
                    fileMap, Constants.NOTIFICATION_SUBMIT_FORM_REQUEST);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * onReceiveNotification
     *
     * @param notificationType
     * @param notificationMessage
     */
    @Override
    public void onReceiveNotification(String notificationType, String notificationMessage) {
        super.onReceiveNotification(notificationType, notificationMessage);

        switch (notificationType) {
            case Constants.NOTIFICATION_SUBMIT_FORM_REQUEST:
                validateFormSubmissionResponse(notificationMessage);
                break;

            case Constants.NOTIFICATION_ACTIVITY_ALERT_REQUEST1:
                deleteRecordsFromRoomDatabase(intialPosition);
                break;
        }
    }

    private void DeleteExistedRecordAndMoveToNextRecord() {
    }


    /**
     * validateFormSubmissionResponse
     */
    private void validateFormSubmissionResponse(String response) {
        try {
            if (response != null) {
                JSONObject jsonObject = new JSONObject(response);
                boolean isStatus = jsonObject.getBoolean("Status");
                String message = jsonObject.getString("Messege");
                if (isStatus) {
                    deleteRecordsFromRoomDatabase(intialPosition);
//                    else {
//
//                    }
//                    Utilities.showActionAlertMessageForActivity(message, this, Constants.NOTIFICATION_ACTIVITY_ALERT_REQUEST1);
                } else {
                    // Utilities.showAlertMessage(message, this);
                    Utilities.showActionAlertMessageForActivity("Consumer Number "+userOfflineData.get(intialPosition).getkNo() + " already exist in Database. Please delete to continue to further", this, Constants.NOTIFICATION_ACTIVITY_ALERT_REQUEST1);
                }
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}