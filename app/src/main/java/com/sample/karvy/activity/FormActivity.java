package com.sample.karvy.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sample.karvy.R;
import com.sample.karvy.adapters.OfflineListAdapter;
import com.sample.karvy.common.ActivityBase;
import com.sample.karvy.common.GPSTracker;
import com.sample.karvy.common.UserPreferences;
import com.sample.karvy.model.FormActivityDetailsModel;
import com.sample.karvy.restapi.NetWorkConnection;
import com.sample.karvy.restapi.VolleyHelperMultiPart;
import com.sample.karvy.roomDb.db.PDQuaQuaDBClient;
import com.sample.karvy.roomDb.entities.UserEntity;
import com.sample.karvy.utils.Constants;
import com.sample.karvy.utils.Conversions;
import com.sample.karvy.utils.DatePicker;
import com.sample.karvy.utils.ImageFilePath;
import com.sample.karvy.utils.ServerUrls;
import com.sample.karvy.utils.Utilities;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class FormActivity extends ActivityBase implements View.OnClickListener,
        TextWatcher, DatePickerDialog.OnDateSetListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private TextInputLayout new_meter_input, k_no_input, old_meter_serial_input, old_meter_reading_input, box_seal_no1_input, box_seal_no2_input, instalation_date_input, address_input,
            mobile_number_input, new_meter_image_input, latest_bill_image_input;
    private EditText new_meter, k_no, old_meter_serial, old_meter_reading, box_seal_no1, box_seal_no2, instalation_date, address, mobile_number;
    private LatLng latLng;
    private LocationRequest mLocationRequest;
    private Double latitude, longitude, final_latitude, final_longitude;
    private GoogleApiClient mGoogleApiClient;
    private GPSTracker gp;
    private ImageView newMeter_image, latest_bill_image;
    private boolean isMeterImage;
    private File newMeter_image_file, latest_bill_image_file;
    private List<FormActivityDetailsModel> userOfflineData = new ArrayList<>();
    private FormActivityDetailsModel detailsmodel;
    private String id_proof;
    private RadioGroup radioGroup_1,radioGroup_2;
    private String mobileNumber;
    private boolean isRadioGroup_2_hide=true;
    private boolean consumer_unavailable,consumer_denied;

    /**
     * onCreate
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupLayoutBase(R.layout.activity_form_layout);
        setActionBarTitle("Form", true, false);
        initializingView();
        initializingGoogleApi();
    }

    /**
     * initializingView
     */
    private void initializingView() {
        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            //TextInput Layout
            new_meter_input = findViewById(R.id.new_meeter_no_input);
            k_no_input = findViewById(R.id.K_No_input);
            old_meter_serial_input = findViewById(R.id.Old_Meter_Serial_No_input);
            old_meter_reading_input = findViewById(R.id.Old_Meter_Reading_input);
            box_seal_no1_input = findViewById(R.id.Box_Seal_No_1_input);
            box_seal_no2_input = findViewById(R.id.Box_Seal_No_2_input);
            instalation_date_input = findViewById(R.id.Installation_Date_input);
            address_input = findViewById(R.id.Customer_Address_input);
            mobile_number_input = findViewById(R.id.Mobile_No_input);
            new_meter_image_input = findViewById(R.id.New_Meter_Image_input);
            latest_bill_image_input = findViewById(R.id.Latest_Electricity_Bill_Image_input);
            newMeter_image = findViewById(R.id.New_Meter_Image_src);
            newMeter_image.setOnClickListener(this);
            latest_bill_image = findViewById(R.id.Latest_Electricity_Bill_Image_src);
            latest_bill_image.setOnClickListener(this);
            //Edit Text Fields
            new_meter = findViewById(R.id.new_meeter_no);
            new_meter.addTextChangedListener(this);
            k_no = findViewById(R.id.K_No);
            k_no.addTextChangedListener(this);
            old_meter_serial = findViewById(R.id.Old_Meter_Serial_No);
            old_meter_reading = findViewById(R.id.Old_Meter_Reading);
            box_seal_no1 = findViewById(R.id.Box_Seal_No_1);
            box_seal_no1.addTextChangedListener(this);
            box_seal_no2 = findViewById(R.id.Box_Seal_No_2);
            box_seal_no2.addTextChangedListener(this);
            instalation_date = findViewById(R.id.Installation_Date);
            address = findViewById(R.id.Customer_Address);
            address.addTextChangedListener(this);
            mobile_number = findViewById(R.id.Mobile_No);
            mobile_number.addTextChangedListener(this);
            radioGroup_1=findViewById(R.id.radio_group_1);
            radioGroup_2=findViewById(R.id.radio_group_2);
            radioGroup_1.setOnCheckedChangeListener((radioGroup, i) -> {
                switch(i) {
                    case R.id.yes:
                        mobile_number.setText("");
                        isRadioGroup_2_hide=true;
                        mobile_number_input.setVisibility(View.VISIBLE);
                        radioGroup_2.setVisibility(View.GONE);
                        break;

                    case R.id.no:
                        isRadioGroup_2_hide=false;
                        mobile_number_input.setVisibility(View.GONE);
                        radioGroup_2.setVisibility(View.VISIBLE);
                        break;
                }
            });
            radioGroup_2.setOnCheckedChangeListener((radioGroup, i) -> {
                switch(i)
                {
                    case R.id.consumer_not_available:
                        consumer_unavailable=true;
                        mobileNumber="Consumer unavailable";
                        break;

                    case R.id.consumer_denied:
                        consumer_denied=true;
                        mobileNumber="Consumer denied";
                        break;
                }
            });
            findViewById(R.id.New_Meter_Image).setOnClickListener(this);
            findViewById(R.id.Latest_Electricity_Bill_Image).setOnClickListener(this);
            findViewById(R.id.Installation_Date).setOnClickListener(this);
            findViewById(R.id.submit).setOnClickListener(this);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * callDatePickerDialog
     */
    private void callDatePickerDialog() {
        try {
            DatePicker mDatePickerDialogFragment;
            mDatePickerDialogFragment = new DatePicker();
            mDatePickerDialogFragment.show(getSupportFragmentManager(), "DATE PICK");
            // mDatePickerDialogFragment.getDatePicker().setMaxDate(System.currentTimeMillis());
            // DatePickerDialog datePickerDialog = new DatePickerDialog(getApplicationContext(), date, Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH);  //date is dateSetListener as per your code in question
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == this.RESULT_OK) {
                switch (requestCode) {
                    case Constants.NEW_METER_CAMERA_REQUEST_CODE:
                        onCaptureImageResult(data);
                        break;

                    case Constants.BILL_IMAGE_METER_CAMERA_REQUEST_CODE:
                        onCaptureImageResult(data);
                        break;
                }
            }

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.submit:
                gp = new GPSTracker(FormActivity.this);
                if (gp == null) {
                    Toast.makeText(FormActivity.this, "Not able to get the location from your device", Toast.LENGTH_LONG).show();
                } else if (!Utilities.isGpsEnable(FormActivity.this, gp)) {
                    gp.showSettingsAlert();
                } else {
                    validateInputFields();
                }
                break;

            case R.id.New_Meter_Image:
            case R.id.New_Meter_Image_src:
                isMeterImage = true;
                captureNewMeterImage();
                break;

            case R.id.Latest_Electricity_Bill_Image:
            case R.id.Latest_Electricity_Bill_Image_src:
                isMeterImage = false;
                captureBillImage();
                break;

            case R.id.Installation_Date:
                //callDatePickerDialog();
                displayDatePicker();
                break;
        }
    }

    /**
     * onReceiveNotification
     *
     * @param notificationType
     * @param notificationMessage
     */
    @Override
    public void onReceiveNotification(String notificationType, String notificationMessage) {
        super.onReceiveNotification(notificationType, notificationMessage);

        switch (notificationType) {
            case Constants.NOTIFICATION_SUBMIT_FORM_REQUEST:
                validateFormSubmissionResponse(notificationMessage);
                break;

            case Constants.NOTIFICATION_ACTIVITY_ALERT_REQUEST1:
                Intent intent = new Intent(this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                finish();
                // clearEditTextFields();
                break;
        }
    }

    /**
     * validateFormSubmissionResponse
     */
    private void validateFormSubmissionResponse(String response) {
        try {
            if (response != null) {
                JSONObject jsonObject = new JSONObject(response);
                boolean isStatus = jsonObject.getBoolean("Status");
                String message = jsonObject.getString("Messege");
                if (isStatus) {
                    if (newMeter_image_file.exists()) {
                        if (newMeter_image_file.delete()) {
                            System.out.println("file Deleted :");
                        }
                    }
                    if (latest_bill_image_file.exists()) {
                        if (latest_bill_image_file.delete()) {
                            System.out.println("file Deleted :");
                        }
                    }

                    Utilities.showActionAlertMessageForActivity(message, this, Constants.NOTIFICATION_ACTIVITY_ALERT_REQUEST1);
                } else {
                    Utilities.showAlertMessage(message, this);
                }
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * validateInputFields
     */
    private void validateInputFields() {
        try {
            boolean isValidate = true;
            if (new_meter.getText().toString().equalsIgnoreCase("")) {
                new Utilities().setErrorToTextInputLayout(new_meter_input, "New Meter No is required");
                isValidate = false;
            } else if (new_meter.getText().toString().trim().length() < 7
                    || new_meter.getText().toString().trim().length() > 7) {
                new Utilities().setErrorToTextInputLayout(new_meter_input, "Please enter valid new meter number");
                isValidate = false;
            }

            if (k_no.getText().toString().equalsIgnoreCase("")) {
                new Utilities().setErrorToTextInputLayout(k_no_input, "K.No is required");
                isValidate = false;
            }

            if (address.getText().toString().trim().equalsIgnoreCase("")) {
                new Utilities().setErrorToTextInputLayout(address_input, "Address is required");
                isValidate = false;
            }

            if (box_seal_no1.getText().toString().equalsIgnoreCase("")) {
                new Utilities().setErrorToTextInputLayout(box_seal_no1_input, "Box Seal No.1 is required");
                isValidate = false;
            } else if (box_seal_no1.getText().toString().trim().length() < 6
                    || box_seal_no1.getText().toString().trim().length() > 6) {
                new Utilities().setErrorToTextInputLayout(box_seal_no1_input, "Please enter valid Box Seal No.1");
                isValidate = false;
            }


            if (!box_seal_no2.getText().toString().equalsIgnoreCase("")) {
                if (box_seal_no2.getText().toString().trim().length() < 6
                        || box_seal_no2.getText().toString().trim().length() > 6) {
                    new Utilities().setErrorToTextInputLayout(box_seal_no2_input, "Please enter valid Box Seal No.2");
                    isValidate = false;
                }
            }

            if(isRadioGroup_2_hide){
                if (mobile_number.getText().toString().equalsIgnoreCase("")) {
                    new Utilities().setErrorToTextInputLayout(mobile_number_input, "Mobile number is required");
                    isValidate = false;
                } else  if (mobile_number.getText().toString().trim().length() < 10
                        || mobile_number.getText().toString().trim().length() > 10) {
                    new Utilities().setErrorToTextInputLayout(mobile_number_input, getString(R.string.mobilenumber_10digits));
                    isValidate = false;
                }
            }else{
                if(!consumer_unavailable && !consumer_denied) {
                    Toast.makeText(this, "Please select below any one of the option if consumer mobile number unavailable", Toast.LENGTH_SHORT).show();
                    isValidate=false;
                }
            }

            if (instalation_date.getText().toString().equalsIgnoreCase("")) {
                new Utilities().setErrorToTextInputLayout(instalation_date_input, "Installation Date is required");
                isValidate = false;
            }

            if (newMeter_image_file == null) {
                new Utilities().setErrorToTextInputLayout(new_meter_image_input, "New Meter Image required");
                isValidate = false;
            }

            if (latest_bill_image_file == null) {
                new Utilities().setErrorToTextInputLayout(latest_bill_image_input, "Latest Electricity Bill Image is required");
                isValidate = false;
            }

            if (isValidate) {
                submitDetailsToServer();
            }

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * captureNewMeterImage
     */
    private void captureNewMeterImage() {
        try {
            checkingCameraPermissions();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * captureBillImage
     */
    private void captureBillImage() {
        try {
            checkingCameraPermissions();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * Checking Camera Permissions
     */
    private void checkingCameraPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkCameraManifestPermissionSets();
        } else {
            if (isMeterImage) {
                chooseNewMeterImageFromCamera();
            } else {
                chooseBillImageFromCamera();
            }
        }
    }

    /**
     * checkManifestPermissionSets
     */
    @TargetApi(23)
    private void checkCameraManifestPermissionSets() {

        int cameraPermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);

        int readPermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        int writePermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        List<String> permissions = new ArrayList<String>();

        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.CAMERA);
        }

        if (readPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (!permissions.isEmpty()) {
            requestPermissions(
                    permissions.toArray(new String[permissions.size()]),
                    Constants.REQUEST_CODE_CAMERA_PERMISSIONS);
        } else {
            if (isMeterImage) {
                chooseNewMeterImageFromCamera();
            } else {
                chooseBillImageFromCamera();
            }
        }
    }


    /**
     * chooseNewMeterImageFromCamera
     */
    private void chooseNewMeterImageFromCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String folder_main = Constants.CAPTURED_FILES_PATH;
            File f = new File(Environment.getExternalStorageDirectory(), folder_main);
            if (!f.exists()) {
                f.mkdirs();
            }
            id_proof = System.currentTimeMillis() + "id_proof.png";
            String folderPath = "/" + folder_main + "/" + id_proof;
            File file = new File(Environment.getExternalStorageDirectory(), folderPath);
            Uri fileUri = Uri.fromFile(new File(file.getAbsolutePath()));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, Constants.NEW_METER_CAMERA_REQUEST_CODE);
           /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);NEW_METER_CAMERA_REQUEST_CODE
            startActivityForResult(intent, Constants.NEW_METER_CAMERA_REQUEST_CODE);*/
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * chooseBillImageFromCamera
     */
    private void chooseBillImageFromCamera() {

        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String folder_main = Constants.CAPTURED_FILES_PATH;
            File f = new File(Environment.getExternalStorageDirectory(), folder_main);
            if (!f.exists()) {
                f.mkdirs();
            }
            id_proof = System.currentTimeMillis() + "id_proof.png";
            String folderPath = "/" + folder_main + "/" + id_proof;
            File file = new File(Environment.getExternalStorageDirectory(), folderPath);
            Uri fileUri = Uri.fromFile(new File(file.getAbsolutePath()));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, Constants.BILL_IMAGE_METER_CAMERA_REQUEST_CODE);
            //Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            //startActivityForResult(intent, Constants.BILL_IMAGE_METER_CAMERA_REQUEST_CODE);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }


    /**
     * onCaptureImageResult
     *
     * @param
     */
    private void onCaptureImageResult(Intent data) {

        ProgressDialog progressDialog = null;
        try {
            Bitmap watermark_bitmap;
            String selectedFilePath;
            File file = new File(Environment.getExternalStorageDirectory(), "/" + Constants.CAPTURED_FILES_PATH + "/" + id_proof);
            file = Utilities.saveBitmapToFile(file);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = Utilities.rotateBitmap(BitmapFactory.decodeStream(new FileInputStream(file), null, options));
            if (isMeterImage) {
                final_latitude = latitude;
                final_longitude = longitude;
                String lat_long = "Lat: " + final_latitude + " Long: " + final_longitude;
                String address = "Address: " + Utilities.getAddressByGeoCordinates(this, final_latitude, final_longitude);
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                String date_time = "Date&Time: " + df.format(c.getTime());
                String finalAddress;
                if (NetWorkConnection.isConnected()) {
                    finalAddress = address + "\n" + lat_long + "\n" + date_time;
                } else {
                    finalAddress = lat_long + "\n" + date_time;
                }
                watermark_bitmap = Utilities.drawTextToBitmap(this, bitmap, finalAddress);
                Uri contentUri = Utilities.getImageUri1(
                        this, watermark_bitmap);
                selectedFilePath = ImageFilePath.getPath(
                        this, contentUri);
                newMeter_image.setImageBitmap(watermark_bitmap);
                newMeter_image_file = new File(selectedFilePath);
                Utilities.setEmptyError(new_meter_image_input);
            } else {
                Uri contentUri = Utilities.getImageUri1(
                        this, bitmap);
                selectedFilePath = ImageFilePath.getPath(
                        this, contentUri);
                latest_bill_image.setImageBitmap(bitmap);
                latest_bill_image_file = new File(selectedFilePath);
                Utilities.setEmptyError(latest_bill_image_input);
            }

            /******** OLD CODE HERE******/
            /*Bitmap photo;
            Bitmap watermark_bitmap;
            String selectedFilePath;
            if (data.getExtras().get("data") != null) {
                final_latitude = latitude;
                final_longitude = longitude;
                String lat_long = "Lat: " + final_latitude + " Long: " + final_longitude;
                String address = "Address: " + Utilities.getAddressByGeoCordinates(this, final_latitude, final_longitude);
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date_time = "Date&Time: " + df.format(c.getTime());
                String finalAddress = address + "\n" + lat_long + "\n" + date_time;
                photo = (Bitmap) data.getExtras().get("data");
                if (isMeterImage) {
                    watermark_bitmap = Utilities.drawTextToBitmap(this, photo, finalAddress);
                    Uri contentUri = Utilities.getImageUri1(
                            this, watermark_bitmap);
                    selectedFilePath = ImageFilePath.getPath(
                            this, contentUri);
                    newMeter_image.setImageBitmap(watermark_bitmap);
                    newMeter_image_file = new File(selectedFilePath);
                    Utilities.setEmptyError(new_meter_image_input);
                } else {
                    Uri contentUri = Utilities.getImageUri1(
                            this, photo);
                    selectedFilePath = ImageFilePath.getPath(
                            this, contentUri);
                    latest_bill_image.setImageBitmap(photo);
                    latest_bill_image_file = new File(selectedFilePath);
                    Utilities.setEmptyError(latest_bill_image_input);
                }
            }
           /* if (isMeterImage) {
                newMeter_image.setImageBitmap(watermark_bitmap);
                newMeter_image_file = new File(selectedFilePath);
                Utilities.setEmptyError(new_meter_image_input);
            } else {
                latest_bill_image.setImageBitmap(photo);
                latest_bill_image_file = new File(selectedFilePath);
                Utilities.setEmptyError(latest_bill_image_input);
            }*/
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * initializingGoogleApi
     */
    private void initializingGoogleApi() {
        try {
            int currentApiVersion = Build.VERSION.SDK_INT;
            if (currentApiVersion >= Build.VERSION_CODES.M) {
                checkLocationManifestPermissionSets();
            } else {
                registerGoogleClient();
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        location.getAccuracy();
        latLng = new LatLng(location.getLatitude(), location.getLongitude());
        latitude = latLng.latitude;
        longitude = latLng.longitude;
        //Toast.makeText(FormActivity.this, "Lat-> " + latitude + "Long-> " + longitude, Toast.LENGTH_SHORT).show();
    }

    /**
     * checkManifestPermissionSets
     * checkManifestPermissionSets
     */
    @TargetApi(23)
    private void checkLocationManifestPermissionSets() {

        int fineLocationPermission = FormActivity.this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        int accessCoarseLocationPermission = FormActivity.this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
        List<String> permissions = new ArrayList<String>();

        if (fineLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (accessCoarseLocationPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!permissions.isEmpty()) {
            requestPermissions(
                    permissions.toArray(new String[permissions.size()]),
                    Constants.REQUEST_CODE_LOCATION_PERMISSIONS);
        } else {
            registerGoogleClient();
        }
    }


    /**
     * registerGoogleClient
     */
    private void registerGoogleClient() {

        try {
            // Create an instance of GoogleAPIClient.
            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(FormActivity.this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .build();

                LocationRequest locationRequest = LocationRequest.create();
                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
                builder.addLocationRequest(locationRequest);
                builder.setAlwaysShow(true);
                //locationSettingsRequest = builder.build();
                connectGoogleApiClient();
            }

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * @param i
     */
    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    /**
     * @param connectionResult
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * onStart
     */
    @Override
    public void onStart() {
        super.onStart();
        try {
            connectGoogleApiClient();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }


    private void connectGoogleApiClient() {
        try {
            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private void disConnectGoogleApiClient() {

        try {
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * onStop
     */
    @Override
    public void onStop() {
        super.onStop();
        disConnectGoogleApiClient();
    }


    /**
     * checkManifestLocationPermissionSets
     */
    @TargetApi(23)
    private void checkManifestLocationPermissionSets() {
        List<String> permissions = new ArrayList<>();
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        if (!permissions.isEmpty()) {
            requestPermissions(
                    permissions.toArray(new String[permissions.size()]),
                    Constants.REQUEST_CODE_LOCATION_PERMISSIONS);
            registerGoogleClient();
        } else {
            try {
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }


    /**
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case Constants.REQUEST_CODE_LOCATION_PERMISSIONS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1]
                        == PackageManager.PERMISSION_GRANTED) {
                    registerGoogleClient();
                } else if (ActivityCompat.shouldShowRequestPermissionRationale(FormActivity.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(FormActivity.this,
                                Manifest.permission.ACCESS_FINE_LOCATION)) {
                    dialog();
                } else {
                    settingPermissionDialog();
                }
            }
            break;

            case Constants.REQUEST_CODE_CAMERA_PERMISSIONS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1]
                        == PackageManager.PERMISSION_GRANTED) {
                    if (isMeterImage) {
                        chooseNewMeterImageFromCamera();
                    } else {
                        chooseBillImageFromCamera();
                    }
                } else if (ActivityCompat.shouldShowRequestPermissionRationale(FormActivity.this,
                        Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(FormActivity.this,
                                Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(FormActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    dialog();
                } else {
                    settingPermissionDialog();
                }
            }
            break;

            default: {
                super.onRequestPermissionsResult(requestCode, permissions,
                        grantResults);
            }
        }
    }


    /**
     * dialog
     */
    private void dialog() {
        TextView allow;
        TextView exit;
        final Dialog dialogtxt = new Dialog(FormActivity.this);
        dialogtxt.setContentView(R.layout.permisiondailog);
        allow = dialogtxt.findViewById(R.id.allow);
        exit = dialogtxt.findViewById(R.id.exit);

        dialogtxt.show();
        allow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogtxt.dismiss();
                checkManifestLocationPermissionSets();
            }
        });
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogtxt.dismiss();
                finish();
            }
        });
    }

    private void dialog1() {
        TextView allow;
        final Dialog dialogtxt = new Dialog(FormActivity.this);
        dialogtxt.setContentView(R.layout.permisiondailog1);
        allow = dialogtxt.findViewById(R.id.allow);
        dialogtxt.show();
        allow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogtxt.dismiss();
                finish();
            }
        });
    }


    /**
     * settingPermissionDialog
     */
    private void settingPermissionDialog() {
        TextView allow;
        TextView exit;
        final Dialog dialogtxt = new Dialog(FormActivity.this);
        dialogtxt.setCancelable(false);
        dialogtxt.setContentView(R.layout.permisiondailog);
        allow = dialogtxt.findViewById(R.id.allow);
        exit = dialogtxt.findViewById(R.id.exit);
        dialogtxt.show();
        allow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogtxt.dismiss();
                Intent i = new Intent();
                i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                i.addCategory(Intent.CATEGORY_DEFAULT);
                i.setData(Uri.parse("package:" + FormActivity.this.getPackageName()));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                FormActivity.this.startActivity(i);
            }
        });
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogtxt.dismiss();
                finish();
            }
        });
    }

    /**
     * @param connectionHint
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this); //Import should not be **android.Location.LocationListener**
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(1000); //1 seconds
            mLocationRequest.setFastestInterval(1000); //1 seconds
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            //mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter
            if (ActivityCompat.checkSelfPermission(FormActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(FormActivity.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * submitDetailsToServer
     */
    private void submitDetailsToServer() {
        try {
            if (final_latitude == null || final_longitude == null) {
                dialog1();
                return;
            }
            if (NetWorkConnection.isConnected())
                syncDetailsToServer();
            else
                saveDataToOfflineRoomStorage();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }


    /**
     * InsertAsyncTask
     */
    private class InsertAsyncTask extends AsyncTask<UserEntity, Void, UserEntity> {

        @Override
        protected void onPostExecute(UserEntity userEntity) {
            super.onPostExecute(userEntity);
            Toast.makeText(FormActivity.this, "Your data successfully stored in offline.",
                    Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(FormActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
            // clearEditTextFields();
        }

        @Override
        protected UserEntity doInBackground(UserEntity... userEntities) {
            if (userEntities != null)
                PDQuaQuaDBClient.getInstance(FormActivity.this)
                        .getAppDatabase()
                        .userDao().insertAll(userEntities);
            return null;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (!(s.toString().equals(""))) {
            //set the Error as null values when user typing:-
            Utilities.setEmptyError(new_meter, s, new_meter_input);
            Utilities.setEmptyError(k_no, s, k_no_input);
            Utilities.setEmptyError(address, s, address_input);
            Utilities.setEmptyError(old_meter_serial, s, old_meter_serial_input);
            Utilities.setEmptyError(old_meter_reading, s, old_meter_reading_input);
            Utilities.setEmptyError(address, s, address_input);
            Utilities.setEmptyError(box_seal_no1, s, box_seal_no1_input);
            Utilities.setEmptyError(box_seal_no2, s, box_seal_no2_input);
            Utilities.setEmptyError(mobile_number, s, mobile_number_input);
        }
    }

    @Override
    public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {
        Calendar mCalender = Calendar.getInstance();
        mCalender.set(Calendar.YEAR, year);
        mCalender.set(Calendar.MONTH, month);
        mCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String selectedDate = DateFormat.getDateInstance(DateFormat.FULL).format(mCalender.getTime());
        instalation_date.setText(selectedDate);
        Utilities.setEmptyError(instalation_date_input);
    }

    private void saveDataToOfflineRoomStorage() {
        try {
            instalation_date = findViewById(R.id.Installation_Date);
            address = findViewById(R.id.Customer_Address);
            mobile_number = findViewById(R.id.Mobile_No);
            detailsmodel = new FormActivityDetailsModel();
            detailsmodel.setNewMettorNo("GP" + new_meter.getText().toString());
            detailsmodel.setkNo(k_no.getText().toString());
            detailsmodel.setOldMeterSerilalNo(old_meter_serial.getText().toString());
            detailsmodel.setOldMeterReading(old_meter_reading.getText().toString());
            detailsmodel.setBoxSealNo1("AN" + box_seal_no1.getText().toString());
            detailsmodel.setBoxSealNo2(!box_seal_no2.getText().toString().equalsIgnoreCase("") ? "AN" + box_seal_no2.getText().toString() : box_seal_no2.getText().toString());
            detailsmodel.setInstallationDate(instalation_date.getText().toString());
            detailsmodel.setCustomerAddress(address.getText().toString());
            if(isRadioGroup_2_hide){
                detailsmodel.setMobileNumber(mobile_number.getText().toString());
            }else{
                detailsmodel.setMobileNumber(mobileNumber);
            }
            detailsmodel.setLatitude(final_latitude);
            detailsmodel.setLongitude(final_longitude);
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDate = df.format(c.getTime());
            detailsmodel.setCurrentDate(currentDate);
            detailsmodel.setNew_meter_image(String.valueOf(newMeter_image_file));
            detailsmodel.setLatest_bill_image(String.valueOf(latest_bill_image_file));
            //Check Either Consumer number exists or not in SQLite Database
            new FormActivity.GetAsyncTask().execute();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     *
     */
    private void syncDetailsToServer() {
        try {
            //Set Headers
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDate = df.format(c.getTime());
            Map<String, String> headers = new HashMap<String, String>();
            headers.put("NewMeter_Number", "GP" + new_meter.getText().toString());
            headers.put("ConsumerNumber", k_no.getText().toString());
            headers.put("OldMeter_Number", old_meter_serial.getText().toString());
            headers.put("OldMeter_Reading", old_meter_reading.getText().toString());
            headers.put("BoxSeal1", "AN" + box_seal_no1.getText().toString());
            headers.put("BoxSeal2", !box_seal_no2.getText().toString().equalsIgnoreCase("") ? "AN" + box_seal_no2.getText().toString() : box_seal_no2.getText().toString());
            headers.put("Address", address.getText().toString());
            if(isRadioGroup_2_hide){
                headers.put("MobileNumber", mobile_number.getText().toString());
            }else{
                headers.put("MobileNumber", mobileNumber);
            }
            headers.put("Latitude", "" + final_latitude);
            headers.put("Longitude", "" + final_longitude);
            headers.put("InstalledDate", instalation_date.getText().toString());
            headers.put("CrtDate", currentDate);

            HashMap<String, List<File>> fileMap = new HashMap();
            //Set Images
            List<File> NewMeterImagesFileList = new ArrayList<>();
            NewMeterImagesFileList.add(newMeter_image_file);
            fileMap.put("newMeterImage", NewMeterImagesFileList);

            List<File> BillImagesFileList = new ArrayList<>();
            BillImagesFileList.add(latest_bill_image_file);
            fileMap.put("latest_Ele_BillImage", BillImagesFileList);

            new VolleyHelperMultiPart().postMultipartMultipleFiles(this, ServerUrls.FORM_SUBMIT_URL, headers,
                    null,
                    fileMap, Constants.NOTIFICATION_SUBMIT_FORM_REQUEST);
        } catch (Throwable t) {

            t.printStackTrace();
        }
    }

    private void displayDatePicker() {
        int mYear, mMonth, mDay, mHour, mMinute;
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, month, dayOfMonth) -> {
                    instalation_date.setText("" + year + "-" + (month + 1) + "-" + dayOfMonth);
                    Utilities.setEmptyError(instalation_date_input);
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void clearEditTextFields() {
        try {

            new_meter.setText("");
            k_no.setText("");
            old_meter_serial.setText("");
            old_meter_reading.setText("");
            box_seal_no1.setText("");
            box_seal_no2.setText("");
            instalation_date.setText("");
            address.setText("");
            mobile_number.setText("");
            new_meter.setText("");
            newMeter_image_file = null;
            latest_bill_image_file = null;
            newMeter_image.setImageResource(R.drawable.image_place_holder);
            latest_bill_image.setImageResource(R.drawable.image_place_holder);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * GetAsyncTask
     */
    private class GetAsyncTask extends AsyncTask<Void, Void, List<UserEntity>> {

        @Override
        protected List<UserEntity> doInBackground(Void... voids) {
            List<UserEntity> list = PDQuaQuaDBClient.getInstance(FormActivity.this)
                    .getAppDatabase()
                    .userDao()
                    .getAllRecords();
            return list;
        }

        @Override
        protected void onPostExecute(List<UserEntity> userEntities) {
            super.onPostExecute(userEntities);
            userOfflineData = new ArrayList<>();
            Type type = new TypeToken<FormActivityDetailsModel>() {
            }.getType();
            if (userEntities != null && userEntities.size() > 0) {
                for (int i = 0; i < userEntities.size(); i++) {
                    FormActivityDetailsModel model = new Gson().fromJson(userEntities.get(i).getUserData().toString(), type);
                    model.setUserId(userEntities.get(i).getId());
                    userOfflineData.add(model);
                }
            }
            //Check user Offline data having Consumer number duplicates....
            boolean isExists = false;
            for (int i = 0; i < userOfflineData.size(); i++) {
                FormActivityDetailsModel model = userOfflineData.get(i);
                if (model.getkNo().equalsIgnoreCase(k_no.getText().toString())) {
                    isExists = true;
                    break;
                }
            }
            if (!isExists) {
                Gson gson = new Gson();
                String detailsModelString = gson.toJson(detailsmodel);
                UserEntity userEntity = new UserEntity();
                userEntity.userData = detailsModelString;
                new InsertAsyncTask().execute(userEntity);
            } else {
                Utilities.showAlertMessage("Consumer Number Already Exist, please check!", FormActivity.this);
            }
        }
    }
}