package com.sample.karvy.common;

import android.content.Context;
import android.content.Intent;

/**
 * UserNotification
 */
public class UserNotification {

    public static final String NOTIFICATION_RECEIVER = "com.karvy.runnerboys.NOTIFICATION_RECEIVER";
    public static final String NOTIFICATION_FRAGMENT_RECEIVER = "com.com.karvy.runnerboys.FRAGMENT_NOTIFICATION_RECEIVER";
    public static final String NOTIFICATION_TYPE = "NOTIFICATION_TYPE";
    public static final String NOTIFICATION_MESSAGE = "NOTIFICATION_MESSAGE";

    private static Context _Context;

    public static void init(Context context) {
        _Context = context;
    }

    public static void notify(String type, String message) {
        Intent intent = new Intent(NOTIFICATION_RECEIVER);
        intent.putExtra(NOTIFICATION_TYPE, type);
        intent.putExtra(NOTIFICATION_MESSAGE, message);
        _Context.getApplicationContext().sendBroadcast(intent);
    }

    public static void notifyFragment(String type, String message) {

        Intent intent = new Intent(NOTIFICATION_FRAGMENT_RECEIVER);
        intent.putExtra(NOTIFICATION_TYPE, type);
        intent.putExtra(NOTIFICATION_MESSAGE, message);
        _Context.getApplicationContext().sendBroadcast(intent);
    }
}
