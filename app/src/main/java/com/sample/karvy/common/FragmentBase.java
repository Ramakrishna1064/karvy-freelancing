package com.sample.karvy.common;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.util.SparseIntArray;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class FragmentBase extends Fragment {

    private SparseIntArray mErrorString;
    /**
     * Register Notification Receiver
     */
    protected BroadcastReceiver _NotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String notificationType = intent.getStringExtra(UserNotification.NOTIFICATION_TYPE);
            String notificationMessage = intent.getStringExtra(UserNotification.NOTIFICATION_MESSAGE);
            onReceiveNotification(notificationType, notificationMessage);
        }
    };


    /**
     * Receive Notifications
     *
     * @param notificationType
     * @param notificationMessage
     */
    public void onReceiveNotification(String notificationType, String notificationMessage) {
        try {

            switch (notificationType) {

            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            getActivity().registerReceiver(this._NotificationReceiver, new IntentFilter(
                   UserNotification.NOTIFICATION_FRAGMENT_RECEIVER));

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            getActivity().unregisterReceiver(this._NotificationReceiver);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    //**************************Run time Permissions*******************************//


    /**
     * onRequestPermissionsResult
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        for (int permission : grantResults) {
            permissionCheck = permissionCheck + permission;
        }
        if ((grantResults.length > 0) && permissionCheck == PackageManager.PERMISSION_GRANTED) {
            onPermissionsGranted(requestCode);
        } else {
            Snackbar.make(getView().findViewById(android.R.id.content), mErrorString.get(requestCode),
                    Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" + getActivity().getPackageName()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            startActivity(intent);
                        }
                    }).show();
        }
    }


    /**
     * requestAppPermissions
     * @param requestedPermissions
     * @param stringId
     * @param requestCode
     */
    public void requestAppPermissions(final String[] requestedPermissions,
                                      final int stringId, final int requestCode) {
        mErrorString.put(requestCode, stringId);
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        boolean shouldShowRequestPermissionRationale = false;
        for (String permission : requestedPermissions) {
            permissionCheck = permissionCheck + ContextCompat.checkSelfPermission(getActivity(), permission);
            shouldShowRequestPermissionRationale = shouldShowRequestPermissionRationale ||
                    ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission);
        }
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale) {
                Snackbar.make(getView().findViewById(android.R.id.content), stringId,
                        Snackbar.LENGTH_INDEFINITE).setAction("GRANT",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ActivityCompat.requestPermissions(getActivity(),
                                        requestedPermissions, requestCode);
                            }
                        }).show();
            } else {
                ActivityCompat.requestPermissions(getActivity(), requestedPermissions, requestCode);
            }
        } else {
            onPermissionsGranted(requestCode);
        }
    }

    /**
     * onPermissionsGranted
     * @param requestCode
     */
    public void onPermissionsGranted(int requestCode){

    }

    /**
     * onDestroy
     */
    /*@Override public void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = AppController.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }*/
}
