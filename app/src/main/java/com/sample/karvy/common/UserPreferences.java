package com.sample.karvy.common;

import android.content.Context;
import android.content.SharedPreferences;

public class UserPreferences {

    private static SharedPreferences _Preferences;

    public static final String MED_PATIENT_PREFERENCES = "SELFIE_REPORTER_PREFERENCES";

    public static void init(Context context) {
        _Preferences = context.getSharedPreferences(MED_PATIENT_PREFERENCES, Context.MODE_PRIVATE);
    }


    /**
     * @param key
     * @param value
     */
    public static void setPreferenceInt(String key, int value) {

        try {
            SharedPreferences.Editor editor = _Preferences.edit();
            editor.putInt(key, value);
            editor.commit();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * @param key
     * @param defValue
     * @return
     */
    public static int getPreferenceInt(String key, int defValue) {

        int value = 0;
        try {
            value = _Preferences.getInt(key, defValue);
            return value;
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return value;
    }

    /**
     * @param key
     * @param value
     */
    public static void setPreferenceBoolean(String key, Boolean value) {
        try {
            SharedPreferences.Editor editor = _Preferences.edit();
            editor.putBoolean(key, value);
            editor.commit();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * @param key
     * @param defValue
     * @return
     */
    public static Boolean getPreferenceBoolean(String key, Boolean defValue) {
        Boolean value = false;
        try {
            value = _Preferences.getBoolean(key, defValue);
            return value;
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return value;
    }

    /**
     * @param key
     * @param value
     */
    public static void setPreferenceString(String key, String value) {

        try {
            SharedPreferences.Editor editor = _Preferences.edit();
            editor.putString(key, value);
            editor.commit();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * @param key
     * @param defValue
     * @return
     */
    public static String getPreferenceString(String key, String defValue) {
        String value = null;
        try {
            value = _Preferences.getString(key, defValue);
            return value;
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return value;
    }


    /**
     * @param key
     * @param value
     */
    public static void setPreferenceLong(String key, long value) {

        try {
            SharedPreferences.Editor editor = _Preferences.edit();
            editor.putLong(key, value);
            editor.commit();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * @param key
     * @param defValue
     * @return
     */
    public static long getPreferenceLong(String key, long defValue) {
        long value = 0;
        try {
            value = _Preferences.getLong(key, defValue);
            return value;
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return value;
    }
}
