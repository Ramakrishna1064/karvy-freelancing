package com.sample.karvy.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.material.appbar.AppBarLayout;
import com.sample.karvy.R;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

/**
 * Created by Ramakrishna...
 */
public class ActivityBase extends AppCompatActivity {

    private View _CoordinatorLayoutView;
    private Toolbar _Toolbar;
    private AppBarLayout appBarLayout;
    private ActionBar _actionBar;
    public boolean isBackButtonDisabled = false;

    /**
     * setupLayoutBase
     *
     * @param contentLayout
     */
    protected void setupLayoutBase(int contentLayout) {

        try {

            setContentView(R.layout.activity_base);
            _CoordinatorLayoutView = findViewById(R.id.coordinator);
            isBackButtonDisabled = false;
            ViewStub stub = (ViewStub) findViewById(R.id.main_content);
            stub.setLayoutResource(contentLayout);
            stub.inflate();
            _Toolbar = (Toolbar) findViewById(R.id.toolbar);
            appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
            setSupportActionBar(_Toolbar);
            _actionBar = getSupportActionBar();
            if (_actionBar != null) {
                _actionBar.setHideOnContentScrollEnabled(false);
            }

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * setActionBarTitle
     *
     * @param title
     * @param isenabled
     * @param isActionBarHide
     */
    public void setActionBarTitle(String title, boolean isenabled, boolean isActionBarHide) {

        try {

            if (title != null && _actionBar != null) {

                _actionBar.setTitle(title);
                TextView tv = new TextView(com.sample.karvy.common.ActivityBase.this);

                RelativeLayout.LayoutParams layoutparams =
                        new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);

                tv.setLayoutParams(layoutparams);
                tv.setText(title);
                tv.setTextColor(Color.WHITE);
                tv.setGravity(Gravity.CENTER);
                tv.setTextSize(22);
                _actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                _actionBar.setCustomView(tv);

                if (isenabled) {
                    _actionBar.setDisplayHomeAsUpEnabled(true);
                } else {
                    _actionBar.setDisplayHomeAsUpEnabled(false);
                }

                if (isActionBarHide) {
                    _Toolbar.setVisibility(View.GONE);
                } else {
                    _Toolbar.setVisibility(View.VISIBLE);
                }
            }

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


    /**
     * hideActionBar
     */
    public void hideActionBar() {
        try {
            appBarLayout.setVisibility(View.GONE);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


    /**
     * Register Notification Receiver
     */
    protected BroadcastReceiver _NotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String notificationType = intent.getStringExtra(UserNotification.NOTIFICATION_TYPE);
            String notificationMessage = intent.getStringExtra(UserNotification.NOTIFICATION_MESSAGE);
            onReceiveNotification(notificationType, notificationMessage);
        }
    };

    /**
     * Receive Notifications
     *
     * @param notificationType
     * @param notificationMessage
     */
    public void onReceiveNotification(String notificationType, String notificationMessage) {
        try {
            switch (notificationType) {
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * onPause
     */
    @Override
    protected void onPause() {
        super.onPause();

        try {
            unregisterReceiver(this._NotificationReceiver);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * onResume
     */
    @Override
    protected void onResume() {
        super.onResume();

        try {
            registerReceiver(this._NotificationReceiver, new IntentFilter(
                    UserNotification.NOTIFICATION_RECEIVER));
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * onOptionsItemSelected
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                if (!isBackButtonDisabled) {
                    onBackPressed();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
