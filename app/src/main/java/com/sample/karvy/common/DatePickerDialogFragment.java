package com.sample.karvy.common;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import com.sample.karvy.utils.Constants;
import java.util.Calendar;
import java.util.Date;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by venkat on 7/29/16.
 */

public class DatePickerDialogFragment extends DialogFragment {

    private String chooseDateType;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return displayDateDialogPicker(getActivity());
    }

    private Dialog displayDateDialogPicker(Context context) {

        int mYear, mMonth, mDay;
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        int mymonth = monthOfYear + 1;
                        String mmmMonth = "", mmDate = "";
                        if (mymonth > 0 && mymonth < 10) {
                            mmmMonth = "0" + mymonth;
                        } else {
                            mmmMonth = "" + mymonth;
                        }
                        if (dayOfMonth > 0 && dayOfMonth < 10) {
                            mmDate = "0" + dayOfMonth;
                        } else {
                            mmDate = "" + dayOfMonth;
                        }
                        //YYYY-MM-DD:-
                        String finalDate = mmDate + "/" + (mmmMonth) + "/" + year;
                        Intent intentData = new Intent();
                       // intentData.putExtra(Constants.BUNDLE_PATIENT_DATE, finalDate);
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intentData);
                    }
                }, mYear, mMonth, mDay);


        //Get Bundle Arguments
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            chooseDateType = bundle.getString("Date");
            if (chooseDateType.equalsIgnoreCase("Past")) {
                int year=2017, month=1, day=1;
                Calendar cal = Calendar.getInstance();
                cal.set( mYear, mMonth, mDay, 0, 0, 0 );
                datePickerDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
                //datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
            } else if (chooseDateType.equalsIgnoreCase("Future")) {
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
            }
        }
        datePickerDialog.show();
        return datePickerDialog;
    }
}
