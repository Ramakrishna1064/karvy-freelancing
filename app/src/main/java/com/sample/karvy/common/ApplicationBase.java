package com.sample.karvy.common;

import android.app.Application;
import android.content.Context;

import com.sample.karvy.restapi.NetWorkConnection;

/**
 * Created by Venu on 7/27/2016.
 */
public class ApplicationBase extends Application {

    private static Context _Context;

    public static Context getContext() {
        return _Context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _Context = getApplicationContext();
        UserPreferences.init(getApplicationContext());
        UserNotification.init(getApplicationContext());
        NetWorkConnection.init(getApplicationContext());
    }
}
