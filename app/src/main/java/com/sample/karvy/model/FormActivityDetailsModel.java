package com.sample.karvy.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FormActivityDetailsModel implements Parcelable {
    private int userId;
    private String newMettorNo;
    private String kNo;
    private String oldMeterSerilalNo;
    private String oldMeterReading;
    private String boxSealNo1;
    private String boxSealNo2;
    private String installationDate;
    private String customerAddress;
    private String MobileNumber;
    private String new_meter_image;
    private String latest_bill_image;
    private Double latitude;
    private Double longitude;

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    private String currentDate;

    public FormActivityDetailsModel() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getNewMettorNo() {
        return newMettorNo;
    }

    public void setNewMettorNo(String newMettorNo) {
        this.newMettorNo = newMettorNo;
    }

    public String getkNo() {
        return kNo;
    }

    public void setkNo(String kNo) {
        this.kNo = kNo;
    }

    public String getOldMeterSerilalNo() {
        return oldMeterSerilalNo;
    }

    public void setOldMeterSerilalNo(String oldMeterSerilalNo) {
        this.oldMeterSerilalNo = oldMeterSerilalNo;
    }

    public String getOldMeterReading() {
        return oldMeterReading;
    }

    public void setOldMeterReading(String oldMeterReading) {
        this.oldMeterReading = oldMeterReading;
    }

    public String getBoxSealNo1() {
        return boxSealNo1;
    }

    public void setBoxSealNo1(String boxSealNo1) {
        this.boxSealNo1 = boxSealNo1;
    }

    public String getBoxSealNo2() {
        return boxSealNo2;
    }

    public void setBoxSealNo2(String boxSealNo2) {
        this.boxSealNo2 = boxSealNo2;
    }

    public String getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(String installationDate) {
        this.installationDate = installationDate;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }


    public String getNew_meter_image() {
        return new_meter_image;
    }

    public void setNew_meter_image(String new_meter_image) {
        this.new_meter_image = new_meter_image;
    }

    public String getLatest_bill_image() {
        return latest_bill_image;
    }

    public void setLatest_bill_image(String latest_bill_image) {
        this.latest_bill_image = latest_bill_image;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public static Creator<FormActivityDetailsModel> getCREATOR() {
        return CREATOR;
    }

    protected FormActivityDetailsModel(Parcel in) {
        userId = in.readInt();
        newMettorNo = in.readString();
        kNo = in.readString();
        oldMeterSerilalNo = in.readString();
        oldMeterReading = in.readString();
        boxSealNo1 = in.readString();
        boxSealNo2 = in.readString();
        installationDate = in.readString();
        customerAddress = in.readString();
        MobileNumber = in.readString();
        new_meter_image = in.readString();
        latest_bill_image = in.readString();
        if (in.readByte() == 0) {
            latitude = null;
        } else {
            latitude = in.readDouble();
        }
        if (in.readByte() == 0) {
            longitude = null;
        } else {
            longitude = in.readDouble();
        }
    }

    public static final Creator<FormActivityDetailsModel> CREATOR = new Creator<FormActivityDetailsModel>() {
        @Override
        public FormActivityDetailsModel createFromParcel(Parcel in) {
            return new FormActivityDetailsModel(in);
        }

        @Override
        public FormActivityDetailsModel[] newArray(int size) {
            return new FormActivityDetailsModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userId);
        dest.writeString(newMettorNo);
        dest.writeString(kNo);
        dest.writeString(oldMeterSerilalNo);
        dest.writeString(oldMeterReading);
        dest.writeString(boxSealNo1);
        dest.writeString(boxSealNo2);
        dest.writeString(installationDate);
        dest.writeString(customerAddress);
        dest.writeString(MobileNumber);
        dest.writeString(new_meter_image);
        dest.writeString(latest_bill_image);
        if (latitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(latitude);
        }
        if (longitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(longitude);
        }
    }
}