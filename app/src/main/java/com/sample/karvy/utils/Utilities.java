package com.sample.karvy.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.Layout;
import android.text.SpannableString;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.google.android.material.textfield.TextInputLayout;
import com.sample.karvy.R;
import com.sample.karvy.activity.LoginActivity;
import com.sample.karvy.common.GPSTracker;
import com.sample.karvy.common.UserNotification;
import com.sample.karvy.common.UserPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.core.content.ContextCompat;

/**
 * Created by Ram on 7/29/2016.
 */
public class Utilities {

    /**
     * @return
     */
    public static ProgressDialog showProgressDialog(Context context) {

        ProgressDialog dialog = new ProgressDialog(context);
        try {
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
        }
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.progressdialog, null, false);
        dialog.setContentView(view);
        return dialog;
    }


    /**
     * @param message
     */
    public static void showAlertMessage(String message, Context context) {

        new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        }).show();


    }

    /**
     * @param message
     * @param context
     * @param notificationType
     */
    public static void showActionAlertMessageForActivity(String message, Context context, final String notificationType) {

        new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                                UserNotification.notify(notificationType, "");
                            }
                        }).show();

    }

    public static void showActionAlertMessageForFragmentForceUpdate(String message, Context context, final String notificationType) {

        new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                                UserNotification.notifyFragment(notificationType, "");
                            }
                        })
                .show();

    }

    /**
     * @param context
     */
    public static void showSessionExpiredDialog(final Context context) {

        try {

            new AlertDialog.Builder(context)
                    .setTitle("Alert")
                    .setMessage(context.getResources().getString(R.string.session_expired))
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.ok,
                            (dialog, which) -> {
                                dialog.dismiss();
                                callLoginActivity(context);
                            }).show();

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


    /**
     * setErrorToTextInputLayout
     *
     * @param textInputLayout
     * @param message
     */
    public void setErrorToTextInputLayout(TextInputLayout textInputLayout, String message) {

        try {

            textInputLayout.setError(message);
            //textInputLayout.setErrorEnabled(true);
            if (textInputLayout.getChildCount() == 2)
                textInputLayout.getChildAt(1).setVisibility(View.VISIBLE);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * setEmptyError
     *
     * @param inputLayout
     */
    public static void setEmptyError(TextInputLayout inputLayout) {

        try {
            if (inputLayout != null) {
                inputLayout.setError(null);
                //inputLayout.setErrorEnabled(false);
                if (inputLayout.getChildCount() == 2)
                    inputLayout.getChildAt(1).setVisibility(View.GONE);
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * setEmptyError for AutoCompleteTextView
     */
    public static void setEmptyError(AutoCompleteTextView autoCompleteTextView,
                                     Editable s, TextInputLayout textInputLayout) {

        try {

            if (autoCompleteTextView.getText().hashCode() == s.hashCode()) {
                //editText.setError(null);
                if (textInputLayout != null) {
                    textInputLayout.setError(null);
                    //inputLayout.setErrorEnabled(false);
                    if (textInputLayout.getChildCount() == 2)
                        textInputLayout.getChildAt(1).setVisibility(View.GONE);
                }
            }

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * @param /setEmptyError for EditText
     * @param /set           EmptyError for EditText
     */
    public static void setEmptyError(EditText editText, Editable s, TextInputLayout inputLayout) {

        try {
            if (editText.getText().hashCode() == s.hashCode()) {
                //editText.setError(null);
                if (inputLayout != null) {
                    inputLayout.setError(null);
                    //inputLayout.setErrorEnabled(false);
                    if (inputLayout.getChildCount() == 2)
                        inputLayout.getChildAt(1).setVisibility(View.GONE);
                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * @param phone
     * @return "true" if target is valid, else "false"
     * @author Chandar Rao
     * @description This method is used to check if the specified mobile number is valid.
     */
    public final static boolean isValidMobile(String phone) {
        if (phone.length() != 10) {
            return false;
        } else if (phone.startsWith("0")) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(phone).matches();
        }
    }

    /**
     * @param arrayList
     * @param autoCompleteTextView
     */
    public static void setAutoCompleteTextViewAdapter(Context context, ArrayList<String> arrayList,
                                                      AutoCompleteTextView autoCompleteTextView) {

        try {
            int layoutItemId = android.R.layout.simple_dropdown_item_1line;
            ArrayAdapter<String> adapter = new ArrayAdapter<>(context, layoutItemId, arrayList);
            autoCompleteTextView.setAdapter(adapter);
            autoCompleteTextView.setThreshold(0);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * @param hm
     * @param value
     * @return
     */
    public static Object getKeyFromValue(Map hm, Object value) {

        for (Object o : hm.keySet()) {
            if (hm.get(o).equals(value)) {
                return o;
            }
        }
        return null;
    }

    /**
     * @param context
     */
    public static void callLoginActivity(Context context) {
        try {
            UserPreferences.setPreferenceString(Constants.USER_ID, null);
            UserPreferences.setPreferenceString(Constants.ATHERIZATION_TOKEN, null);
            Intent i = new Intent(context, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(i);
            ((Activity) context).finish();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * Getting path of captured image in android using camera intent
     *
     * @param context
     * @param inImage
     * @return
     */

    public static Uri getImageUri(Context context, Bitmap inImage) {

        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(context.getContentResolver(),
                    inImage, "Title", null);
            return Uri.parse(path);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    public static Uri getImageUri1(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "IMG_" + Calendar.getInstance().getTime(), null);
        return Uri.parse(path);
    }


    /**
     * @param uri
     * @param context
     * @return
     */
    public static String getRealPathFromURI(Uri uri, Context context) {

        try {
            Cursor cursor = context.getContentResolver().query(uri, null,
                    null, null, null);
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    /**
     * @param
     * @param context
     * @return
     */
    public static Bitmap getImagePathFromUri(Uri selectedImageUri, Context context) {

        try {

            String[] projection = {MediaStore.MediaColumns.DATA};

            Cursor cursor = context.getContentResolver().query(selectedImageUri, projection, null, null, null);

            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();

            String selectedImagePath = cursor.getString(column_index);

            Bitmap bitmap;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(selectedImagePath, options);
            final int REQUIRED_SIZE = 200;
            int scale = 1;
            while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                    && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeFile(selectedImagePath, options);

            return bitmap;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }


    /**
     * @param textview
     * @param message
     */
    public static void underLineTextView(TextView textview, String message) {

        try {
            SpannableString content = new SpannableString(message);
            content.setSpan(new UnderlineSpan(), 0, message.length(), 0);
            textview.setText(content);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }


    /**
     * @param ipDate
     * @return
     */
    public static String setAvailableDateFormat(String ipDate) {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
            SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy");
            Date d = sdf.parse(ipDate);
            String formattedTime = output.format(d);
            return formattedTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * splitTheDistance
     *
     * @param dst
     */
    public static Double splitTheDistance(String dst) {
        String[] otpMessageTokensArr = dst.split(" ");
        return Double.parseDouble(otpMessageTokensArr[0]);
    }


    /**
     * convertToHashMap
     *
     * @param jsonString
     * @return
     */
    public static HashMap<String, String> convertToHashMap(String jsonString) {

        HashMap<String, String> myHashMap = new HashMap<String, String>();
        try {
            JSONArray jArray = new JSONArray(jsonString);
            JSONObject jObject = null;
            String keyString = null;
            System.out.println("JsonArray size----->" + jArray.length());
            for (int i = 0; i < jArray.length(); i++) {
                jObject = jArray.getJSONObject(i);
                keyString = (String) jObject.names().get(0);
                myHashMap.put(keyString, jObject.getString(keyString));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return myHashMap;
    }

    /**
     * showSnackBarMessage
     */
    public static void showSnackBarMessage(int layout, String message) {

        try {

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public static void DeleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) for (File child : fileOrDirectory.listFiles())
            DeleteRecursive(child);
        fileOrDirectory.delete();
    }

    /**
     * deleteFileFromMediaStore
     *
     * @param contentResolver
     * @param file
     */
    public static void deleteFileFromMediaStore(final ContentResolver contentResolver, final File file) {

        String canonicalPath;
        try {
            canonicalPath = file.getCanonicalPath();
        } catch (IOException e) {
            canonicalPath = file.getAbsolutePath();
        }
        final Uri uri = MediaStore.Files.getContentUri("external");
        final int result = contentResolver.delete(uri,
                MediaStore.Files.FileColumns.DATA + "=?", new String[]{canonicalPath});
        if (result == 0) {
            final String absolutePath = file.getAbsolutePath();
            if (!absolutePath.equals(canonicalPath)) {
                contentResolver.delete(uri,
                        MediaStore.Files.FileColumns.DATA + "=?", new String[]{absolutePath});
            }
        }
    }


    /**
     * SaveImage
     *
     * @param context
     * @param
     */
    public static File saveImage(Context context, Intent data) {

        File mediaFile = null;
        try {
            Bitmap imgBitmap = (Bitmap) data.getExtras().get("data");
            File sd = Environment.getExternalStorageDirectory();
            File imageFolder = new File(sd.getAbsolutePath() + File.separator +
                    ".FOSImages");
            if (!imageFolder.isDirectory()) {
                imageFolder.mkdirs();
            }
            mediaFile = new File(imageFolder + File.separator + "fos_" +
                    System.currentTimeMillis() + ".jpg");
            FileOutputStream fileOutputStream = new FileOutputStream(mediaFile);
            imgBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
            fileOutputStream.close();
            return mediaFile;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return mediaFile;
    }


    /**
     * getStringFileToBase64
     *
     * @param f
     * @return
     */
    public static String getStringFileToBase64(File f) {

        InputStream inputStream = null;
        String encodedFile = "", lastVal;
        try {
            inputStream = new FileInputStream(f.getAbsolutePath());
            byte[] buffer = new byte[10240];//specify the size to allow
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }
            output64.close();
            encodedFile = output.toString();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        lastVal = encodedFile;
        return lastVal;
    }


    /**
     * compressImage
     *
     * @param imageUri
     * @return
     */
    public static String compressImage(Context context, String imageUri) {

        String filePath = getRealPathFromURI(context, imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;
    }


    public static String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "FOSImages");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private static String getRealPathFromURI(Context context, String contentURI) {

        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public static void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    public static boolean isMockLocationOn(Location location, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            try {
                return location.isFromMockProvider();
            } catch (Exception e) {
                return false;
            }

        } else {
           /* String mockLocation = "0";
            try {
                mockLocation = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ALLOW_MOCK_LOCATION);
            } catch (Exception e) {
                e.printStackTrace();
                return  false;
            }
            return !mockLocation.equals("0");*/
            return false;
        }
    }

    /**
     * @param message
     */
    public static void showAlertMessageWithNoAction(String message, Context context, final String notificationType) {

        new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                                UserNotification.notifyFragment(notificationType, "");
                                UserNotification.notify(notificationType, "");
                            }
                        }).show();


    }

    /**
     * @param message
     * @param context
     */
    public static void errorDialog(String message, String title, Context context) {
        TextView errormsgtext;
        TextView errortitletext;
        Button btn = null;
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.errordailog);
        errortitletext = dialog.findViewById(R.id.error_txt);

        errormsgtext = dialog.findViewById(R.id.error_des);
        errortitletext.setText(title);
        errormsgtext.setText(message);
        btn = dialog.findViewById(R.id.error_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.show();
    }

    /**
     * isGpsEnable
     *
     * @param context
     * @param gps
     * @return
     */
    public static boolean isGpsEnable(Context context, GPSTracker gps) {

        int currentApiVersion = Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.M) {
            if (gps.canGetLocation()) {
                return true;
            } else {
                return false;
            }
        } else {
            String provider = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (!provider.contains("gps")) {
                return false;
            } else {
                return true;
            }
        }
    }

    public static String getVersionName(Context context) {
        try {
            return context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return "";
    }

    public static String convertToDDMMYYYYFormat(String dateToConvert) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = inputFormat.parse(dateToConvert);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = outputFormat.format(date);
        System.out.println(formattedDate); // prints 10-04-2018
        return formattedDate;
    }

    /**
     * @param src
     * @param watermark
     * @param underline
     * @return
     */
    public static Bitmap mark(Context context, Bitmap src, String watermark, boolean underline) {
        int w = src.getWidth();
        int h = src.getHeight();
        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());

        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(src, 0, 0, null);

        Paint paint = new Paint();
        int color = ContextCompat.getColor(context, R.color.black);
        paint.setColor(color);
        paint.setAlpha(1);
        paint.setTextSize(5);
        paint.setAntiAlias(true);
        paint.setUnderlineText(underline);
        //canvas.drawText(watermark, location.x, location.y, paint);
        canvas.drawText(watermark, canvas.getWidth() / 2, canvas.getHeight() / 2, paint);
        return result;
    }

    /**
     * drawTextToBitmap
     *
     * @param
     * @param
     * @return
     */
    /*public static Bitmap drawTextToBitmap(Context mContext, Bitmap bitmap, String mText) {
        try {
            Resources resources = mContext.getResources();
            float scale = resources.getDisplayMetrics().density;
            android.graphics.Bitmap.Config bitmapConfig = bitmap.getConfig();
            // set default bitmap config if none
            if (bitmapConfig == null) {
                bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
            }
            // so we need to convert it to mutable one
            bitmap = bitmap.copy(bitmapConfig, true);
            Canvas canvas = new Canvas(bitmap);
            // new antialised Paint
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            // text color - #3D3D3D
            paint.setColor(Color.rgb(0, 0, 0));
            // text size in pixels
            paint.setTextSize((int) (4 * scale));
            // text shadow
            //paint.setShadowLayer(1f, 0f, 1f, Color.RED);
            // text shadow
            paint.setShadowLayer(1f, 0f, 1f, Color.BLACK);
            // draw text to the Canvas center
            Rect bounds = new Rect();
            paint.getTextBounds(mText, 0, mText.length(), bounds);
            //int x = (bitmap.getWidth() - bounds.width())/6;
            //int y = (bitmap.getHeight() + bounds.height())/5;
            canvas.drawText(mText, 0, bitmap.getHeight() - bounds.height(), paint);
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }*/
    public static Bitmap drawTextToBitmap(Context gContext,
                                          Bitmap bitmap,
                                          String gText) {
        Resources resources = gContext.getResources();
        float scale = resources.getDisplayMetrics().density;
        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        // set default bitmap config if none
        if (bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        // so we need to convert it to mutable one
        bitmap = bitmap.copy(bitmapConfig, true);
        Canvas canvas = new Canvas(bitmap);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text color - #3D3D3D
        paint.setColor(Color.WHITE);
        // text size in pixels
        paint.setTextSize((int) (10 * scale));
        // text shadow
        //paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);
        // draw text to the Canvas center
        Rect bounds = new Rect();
        int noOfLines = 0;
        for (String line : gText.split("\n")) {
            noOfLines++;
        }
        paint.getTextBounds(gText, 0, gText.length(), bounds);
        int x = 0;
        int y = (bitmap.getHeight() - bounds.height() * noOfLines);
        Paint mPaint = new Paint();
        mPaint.setColor(ContextCompat.getColor(gContext,
                R.color.transparentBlack));
        int top = (bitmap.getHeight() - bounds.height() * (noOfLines + 1));
        int right = bitmap.getWidth();
        int bottom = bitmap.getHeight();
        canvas.drawRect(0, top, right, bottom, mPaint);
        for (String line : gText.split("\n")) {
            canvas.drawText(line, x, y, paint);
            y += paint.descent() - paint.ascent();
        }
        return bitmap;
    }

    /**
     *
     * @param gContext
     * @param bitmap
     * @param gText
     * @return
     */
    public static Bitmap drawMultilineTextToBitmap(Context gContext, Bitmap bitmap, String gText) {
        // prepare canvas
        Resources resources = gContext.getResources();
        float scale = resources.getDisplayMetrics().density;
        //Bitmap bitmap = BitmapFactory.decodeResource(resources, gResId);
        android.graphics.Bitmap.Config bitmapConfig = bitmap.getConfig();
        // set default bitmap config if none
        if (bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        // so we need to convert it to mutable one
        bitmap = bitmap.copy(bitmapConfig, true);
        Canvas canvas = new Canvas(bitmap);
        TextPaint paint = new TextPaint(Paint.LINEAR_TEXT_FLAG|Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(false);
        paint.setFilterBitmap(false);
        paint.setDither(true);
        paint.setColor(Color.rgb(255, 255, 255));
        paint.setFakeBoldText(true);
        paint.setTextSize(8);
        int textWidth = canvas.getWidth() - (int) (16 * scale);
        // init StaticLayout for text
        StaticLayout textLayout = new StaticLayout(gText, paint,
                canvas.getWidth(), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f,
                false);
        // get height of multiline text
        int textHeight = textLayout.getHeight();
        // get position of text's top left corner
        float x = (bitmap.getWidth() - textWidth);
        float y = (bitmap.getHeight() - textHeight);
        // draw text to the Canvas Left
        canvas.save();
        //canvas.translate(x, y);
        canvas.translate((canvas.getWidth() / 2) - (textLayout.getWidth() / 2), y);
        textLayout.draw(canvas);
        canvas.restore();
        return bitmap;
    }

    public static Rect getTextBackgroundSize(float x, float y, String text, TextPaint paint) {
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        float halfTextLength = paint.measureText(text) / 2 + 5;
        return new Rect((int) (x - halfTextLength), (int) (y + fontMetrics.top), (int) (x + halfTextLength), (int) (y + fontMetrics.bottom));
    }

    /**
     *
     * @param context
     * @param latitude
     * @param longitude
     * @return
     */
    public static String getAddressByGeoCordinates(Context context,Double latitude,Double longitude){
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(context, Locale.getDefault());
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            //String postalCode = addresses.get(0).getPostalCode();
            //String knownName = addresses.get(0).getFeatureName();
            String final_address=address+"\n"+city+","+country;
            return final_address;
        }catch (Throwable t){
            t.printStackTrace();
        }
        return null;
    }

    public static File saveBitmapToFile(File file) {
        try {
            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image
            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();
            // The new size we want to scale to
            int REQUIRED_SIZE = 75;
            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);
            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();
            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);
            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
            return file;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap rotateBitmap(Bitmap image){
        int width=image.getHeight();
        int height=image.getWidth();
        Bitmap srcBitmap=Bitmap.createBitmap(width, height, image.getConfig());
        for (int y=width-1;y>=0;y--)
            for(int x=0;x<height;x++)
                srcBitmap.setPixel(width-y-1, x,image.getPixel(x, y));
        return srcBitmap;
    }
}
