package com.sample.karvy.utils;

/**
 * Created by Ram on 7/28/2016.
 */
public class Constants {

    public static final String CONTENT_TYPE = "application/json";
    public static final String CONTENT_TYPE1 = "application/x-www-form-urlencoded";
    public static final String CHARSET_UTF_8 = "UTF-8";
    public static String VOLLEY_ERROR_SERVER = "There is a problem with the back-end server.";
    public static String VOLLEY_ERROR_INTERNET = "Internet access is required.";
    public static String VOLLEY_ERROR_OTHER = "There was an error with the web request.";

    //Network Checking
    public static String NETWORK_TYPE_WIFI = "NETWORK_TYPE_WIFI";
    public static String NETWORK_TYPE_MOBILE = "NETWORK_TYPE_MOBILE";
    public static String NETWORK_TYPE_NOT_CONNECTED = "NETWORK_TYPE_NOT_CONNECTED";
    public static String NETWORK_STRING_WIFI = "Wifi enabled";
    public static String NETWORK_STRING_MOBILE = "Mobile data enabled";
    public static String NETWORK_STRING_NOT_CONNECTED = "Not connected to Internet";

    //Rest API Request Code
    public static final String NOTIFICATION_LOGIN_REQUEST = "NOTIFICATION_LOGIN_REQUEST";
    public static final String NOTIFICATION_SUBMITTED_RECORDS_LIST_REQUEST = "NOTIFICATION_SUBMITTED_RECORDS_LIST_REQUEST";
    public static final String NOTIFICATION_SUBMIT_FORM_REQUEST = "NOTIFICATION_SUBMIT_FORM_REQUEST";
    public static final String NOTIFICATION_RECORD_EXISTS = "NOTIFICATION_RECORD_EXISTS";

    //Notifications from Alert to Activity
    public static final String NOTIFICATION_ACTIVITY_ALERT_REQUEST1 = "NOTIFICATION_ACTIVITY_ALERT_REQUEST1";

    //Activity Result codes
    public static final String USER_ID = "user_id";
    public static final String USER_EMAIL = "user_email";
    public static final String ATHERIZATION_TOKEN = "ATHERIZATION_TOKEN";

    public static final int REQUEST_CODE_LOCATION_PERMISSIONS = 100;
    public static final int REQUEST_CODE_CAMERA_PERMISSIONS = 101;
    public static final int NEW_METER_CAMERA_REQUEST_CODE = 200;
    public static final int BILL_IMAGE_METER_CAMERA_REQUEST_CODE = 201;

    public static final String CAPTURED_FILES_PATH = ".KARVY_APP";
}

