package com.sample.karvy.utils;

import android.app.Application;
import android.content.Context;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.sample.karvy.common.UserNotification;
import com.sample.karvy.common.UserPreferences;
import com.sample.karvy.restapi.NetWorkConnection;

import androidx.multidex.MultiDex;

public class AppController extends Application {

    public static final String TAG = com.sample.karvy.utils.AppController.class
            .getSimpleName();
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static com.sample.karvy.utils.AppController mInstance;
    private static Context _Context;
    public static Context getContext() {
        return _Context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        _Context = getApplicationContext();
        UserNotification.init(getApplicationContext());
        UserPreferences.init(getApplicationContext());
        NetWorkConnection.init(getApplicationContext());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static synchronized com.sample.karvy.utils.AppController getInstance() {
        return mInstance;
    }
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }
}