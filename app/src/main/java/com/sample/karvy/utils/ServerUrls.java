package com.sample.karvy.utils;

/**
 * Created by chandu on 30-06-2017.
 */
public class ServerUrls {
    public static String BASE_URL = "http://appnet.karvyclick.com/DisCom/api/";
    public static String LOGIN_TOKEN = BASE_URL + "auth/login";
    public static String FORM_SUBMIT_URL = BASE_URL + "Consumer/UpdateConsumerData";
    public static String FORM_SUBMIT_RECORDS_LIST_URL = BASE_URL + "Consumer/GetConsumersByStatOffLine";
}
