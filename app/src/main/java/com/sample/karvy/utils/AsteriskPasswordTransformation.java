package com.sample.karvy.utils;

import android.graphics.Rect;
import android.text.method.TransformationMethod;
import android.view.View;

/* @author Chandar
        * @Class_Name : AsteriskPasswordTransformation
        * @Class_Description : using  enter password field Should be show (*)

        */
public class AsteriskPasswordTransformation implements TransformationMethod {
    @Override
    public CharSequence getTransformation(CharSequence source, View view) {
        return new PasswordCharSequence(source);
    }

    @Override
    public void onFocusChanged(View view, CharSequence sourceText, boolean focused, int direction, Rect previouslyFocusedRect) {

    }

    private class PasswordCharSequence implements CharSequence {
        private CharSequence mSource;

        public PasswordCharSequence(CharSequence source) {
            mSource = source; // Store char sequence
        }

        public char charAt(int index) {
            return '*'; // This is the important part
        }

        public int length() {
            return mSource.length(); // Return default
        }

        public CharSequence subSequence(int start, int end) {
            return mSource.subSequence(start, end); // Return default
        }
    }
};

