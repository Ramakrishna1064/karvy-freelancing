package com.sample.karvy.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by shyam on 4/19/2017.
 */

public class RegularExpressions {


    public static final String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!#$%^&+=])(?=\\S+$).{7,}";
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String MOBILE_NUMBER_PATTERN = "/^(?!0{6})[A-Z0-9][0-9]{5}$/";

    /**
     * Compile the Pattern Matcher
     *
     * @param patternName
     * @param inputText
     * @return
     */

    public static boolean compilePattern(String patternName, String inputText) {

        Pattern pattern = Pattern.compile(patternName);
        Matcher matcher = pattern.matcher(inputText);
        return matcher.matches();
    }


    /**
     * Validate Password Pattern
     *
     * @param password
     * @return
     */
    public static boolean validatePassword(final String password) {

        return compilePattern(PASSWORD_PATTERN, password);
    }


    /**
     * Validate Email Pattern
     *
     * @param email
     * @return
     */
    public static boolean validateEmail(String email) {

        return compilePattern(EMAIL_PATTERN, email);
    }

    /**
     * Validate Email Pattern
     *
     * @param email
     * @return
     */
    public static boolean validateMobileNumber(String email) {
        return compilePattern(MOBILE_NUMBER_PATTERN, email);
    }
}
