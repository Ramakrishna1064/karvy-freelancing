package com.sample.karvy.restApiModel;

import java.util.List;

public class SubmittedRecordsResult {
    private boolean Status;

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public List<SubmittedRecordsList> getResult() {
        return Result;
    }

    public void setResult(List<SubmittedRecordsList> result) {
        Result = result;
    }

    public String getMessege() {
        return Messege;
    }

    public void setMessege(String messege) {
        Messege = messege;
    }

    private List<SubmittedRecordsList> Result;
    private String Messege;
}
