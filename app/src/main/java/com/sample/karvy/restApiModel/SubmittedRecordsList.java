package com.sample.karvy.restApiModel;

public class SubmittedRecordsList {
    private int Id;
    private String ConsumerName;

    public String getConsumerNumber() {
        return ConsumerNumber;
    }

    public void setConsumerNumber(String consumerNumber) {
        ConsumerNumber = consumerNumber;
    }

    private String ConsumerNumber;
    private String NewMeter_Number;

    public String getNewMeter_Number() {
        return NewMeter_Number;
    }

    public void setNewMeter_Number(String newMeter_Number) {
        NewMeter_Number = newMeter_Number;
    }

    private String OldMeter_Number;
    private String OldMeter_Reading;
    private String BoxSeal1;
    private String BoxSeal2;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getConsumerName() {
        return ConsumerName;
    }

    public void setConsumerName(String consumerName) {
        ConsumerName = consumerName;
    }

    public String getOldMeter_Number() {
        return OldMeter_Number;
    }

    public void setOldMeter_Number(String oldMeter_Number) {
        OldMeter_Number = oldMeter_Number;
    }

    public String getOldMeter_Reading() {
        return OldMeter_Reading;
    }

    public void setOldMeter_Reading(String oldMeter_Reading) {
        OldMeter_Reading = oldMeter_Reading;
    }

    public String getBoxSeal1() {
        return BoxSeal1;
    }

    public void setBoxSeal1(String boxSeal1) {
        BoxSeal1 = boxSeal1;
    }

    public String getBoxSeal2() {
        return BoxSeal2;
    }

    public void setBoxSeal2(String boxSeal2) {
        BoxSeal2 = boxSeal2;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getInstalledDate() {
        return InstalledDate;
    }

    public void setInstalledDate(String installedDate) {
        InstalledDate = installedDate;
    }

    private String Address;
    private String MobileNumber;
    private String Email;
    private String Latitude;
    private String Longitude;
    private String InstalledDate;
}
