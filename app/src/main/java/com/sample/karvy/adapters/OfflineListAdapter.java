package com.sample.karvy.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.karvy.R;
import com.sample.karvy.model.FormActivityDetailsModel;

import java.util.List;

public class OfflineListAdapter extends RecyclerView.Adapter<OfflineListAdapter.ViewHolder> {
    Context context;
    private List<FormActivityDetailsModel> userRecords;
    public OfflineListAdapter(Context context, List<FormActivityDetailsModel> userRecords) {
        this.context = context;
        this.userRecords = userRecords;
    }

    @Override
    public OfflineListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.adapter_offline_recyclerview_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(OfflineListAdapter.ViewHolder holder, int position) {
        holder.meterNumber.setText("New Meter No: "+userRecords.get(position).getNewMettorNo());
        holder.kNo.setText("K.NO: "+userRecords.get(position).getkNo());
        holder.installed_on.setText("Installation Date: "+userRecords.get(position).getInstallationDate());
    }


    @Override
    public int getItemCount() {
        return userRecords.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView meterNumber;
        private AppCompatTextView kNo;
        private AppCompatTextView installed_on;

        public ViewHolder(View view) {
            super(view);
            meterNumber = view.findViewById(R.id.meter_number);
            kNo = view.findViewById(R.id.Knumber);
            installed_on = view.findViewById(R.id.installDate);
        }
    }
}