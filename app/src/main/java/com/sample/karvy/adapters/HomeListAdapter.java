
package com.sample.karvy.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.karvy.R;
import com.sample.karvy.restApiModel.SubmittedRecordsList;
import com.sample.karvy.utils.Utilities;


import java.util.List;

public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.ViewHolder> {

    private Context context;
    private List<SubmittedRecordsList> submittedRecordsListList;

    public HomeListAdapter(Context context, List<SubmittedRecordsList> submittedRecordsListList) {
        this.context = context;
        this.submittedRecordsListList = submittedRecordsListList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.adapter_online_recyclerview_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            SubmittedRecordsList submittedObject = submittedRecordsListList.get(position);
            holder.meterNumber.setText("New Meter No: " + submittedObject.getNewMeter_Number());
            holder.kNo.setText("K.NO: " + submittedObject.getConsumerNumber());
            holder.installed_on.setText("Installation Date: " + Utilities.convertToDDMMYYYYFormat(submittedObject.getInstalledDate()));
        }catch(Throwable t){
            t.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return submittedRecordsListList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView meterNumber;
        private AppCompatTextView kNo;
        private AppCompatTextView installed_on;

        public ViewHolder(View view) {
            super(view);
            meterNumber = view.findViewById(R.id.meter_number);
            kNo = view.findViewById(R.id.Knumber);
            installed_on = view.findViewById(R.id.installDate);
        }
    }
}