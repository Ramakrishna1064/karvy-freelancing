package com.sample.karvy.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.sample.karvy.R;
import com.sample.karvy.common.UserNotification;
import com.sample.karvy.model.AttachmentsModel;
import com.sample.karvy.utils.Constants;
import java.io.File;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

public class HorizentalListAdapter extends RecyclerView.Adapter<HorizentalListAdapter.ViewHolder> {

    private Context context;
    private List<AttachmentsModel> attachmentsModelList;

    public HorizentalListAdapter(Context context, List<AttachmentsModel> attachmentsModelList) {

        this.context = context;
        this.attachmentsModelList = attachmentsModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        try {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_attachments_list_item, viewGroup, false);
            return new ViewHolder(v);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        try {
            AttachmentsModel attachmentsModel = attachmentsModelList.get(position);
            File imgFile = new File(attachmentsModel.getFilePath());
            if(imgFile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                viewHolder.imageView.setImageBitmap(myBitmap);
            }
            viewHolder.removeImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // UserNotification.notify(Constants.NOTIFICATION_REMOVE_LIST_ITEM, String.valueOf(position));
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        try {
            return attachmentsModelList.size();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return 0;
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView, removeImage;
        public ViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.grid_image);
            removeImage = (ImageView) view.findViewById(R.id.remove);
        }
    }
}