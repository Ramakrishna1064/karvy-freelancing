package com.sample.karvy.roomDb.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;


import com.sample.karvy.roomDb.entities.UserEntity;

import java.util.List;

@Dao
public interface UserDAO {

    @Insert
    void insertAll(UserEntity... users);

    @Query("SELECT * FROM users")
    List<UserEntity> getAllRecords();

    @Query("DELETE FROM users WHERE user_id = :postId")
    abstract void deleteById(long postId);

    /*@Query("SELECT user_id FROM users WHERE mobile_number=:phone AND first_name = :name")
    int getUserId(String phone, String name);




    @Query("SELECT mobile_number FROM users WHERE mobile_number=:phone AND first_name = :name")
    String getUsername(String phone, String name);*/
}
