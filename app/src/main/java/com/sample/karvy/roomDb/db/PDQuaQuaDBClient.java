package com.sample.karvy.roomDb.db;

import android.content.Context;

import androidx.room.Room;

public class PDQuaQuaDBClient {

    private static PDQuaQuaDBClient mInstance;
    private Context mContext;
    private PDQuaQuaDB appDatabase;

    private PDQuaQuaDBClient(Context mContext) {
        this.mContext = mContext;
        appDatabase = Room.databaseBuilder(mContext, PDQuaQuaDB.class, "PDQuaQuaDB").build();
    }

    public static synchronized PDQuaQuaDBClient getInstance(Context mContext) {
        if (mInstance == null) {
            mInstance = new PDQuaQuaDBClient(mContext);
        }
        return mInstance;
    }

    public PDQuaQuaDB getAppDatabase() {
        return appDatabase;
    }
}
