package com.sample.karvy.roomDb.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.sample.karvy.roomDb.dao.UserDAO;
import com.sample.karvy.roomDb.entities.UserEntity;


@Database(entities = {UserEntity.class}, version = 1, exportSchema = true)
@TypeConverters({DateTypeConverter.class})
public abstract class PDQuaQuaDB extends RoomDatabase {
    public abstract UserDAO userDao();
}